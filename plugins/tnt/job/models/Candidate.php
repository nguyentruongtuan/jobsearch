<?php namespace TNT\Job\Models;

use Model;
use RainLab\User\Models\User;

/**
 * candidate Model
 */
class Candidate extends User
{
	/**
	 * @var string The database table used by the model.
	 */
	protected $table = 'users';

	/**
	 * Validation rules
	 */
	public $rules = [
		'email'    => 'required|between:6,255|email|unique:users',
		'username' => 'required|between:2,255|unique:users',
		'password' => 'required:create|between:4,255|confirmed',
		'password_confirmation' => 'required_with:password|between:4,255'
	];

	/**
	 * @var array Relations
	 */
	public $belongsToMany = [
		'groups' => ['RainLab\User\Models\UserGroup', 'table' => 'users_groups']
	];

	public $attachOne = [
		'avatar' => ['System\Models\File']
	];

	/**
	 * @var array The attributes that are mass assignable.
	 */
	protected $fillable = [
		'name',
		'surname',
		'login',
		'username',
		'email',
		'password',
		'password_confirmation'
	];

	/**
	 * Purge attributes from data set.
	 */
	protected $purgeable = ['password_confirmation', 'send_invite'];

	protected $dates = [
		'last_seen',
		'deleted_at',
		'created_at',
		'updated_at',
		'activated_at',
		'last_login'
	];
}
