<?php namespace TNT\Job\Models;

use Model;

/**
 * settings Model
 */
class Settings extends Model
{
    public $implement = ['System.Behaviors.SettingsModel'];

    // A unique code
    public $settingsCode = 'tnt_jobsearch_settings';

    // Reference to field configuration
    public $settingsFields = 'fields.yaml';
}
