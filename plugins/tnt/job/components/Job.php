<?php namespace TNT\Job\Components;

use Cms\Classes\ComponentBase;
use TNT\Job\Models\Jobcategory;

class Job extends ComponentBase
{
	public $list;

    public function componentDetails()
    {
        return [
            'name'        => 'job Component',
            'description' => 'Display a list of jobs'
        ];
    }

    public function defineProperties()
    {
    	$jobCategories = Jobcategory::all();
    	$arr = [];
    	foreach ($jobCategories as $job_category) {
    		$arr[$job_category->id] = $job_category->name;
	    }

	    return [
		    'category' => [
			    'title'             => 'Job group',
			    'description'       => 'Type of job',
			    'default'           => '',
			    'type'              => 'dropdown',
			    'options'           => $arr
		    ]
	    ];
    }

    public function onRun() {
	    $this->addJs('/plugins/tnt/job/assets/js/job.js');

		$categoryId = $this->property('category');
		$category = Jobcategory::find($categoryId);
		if ($category) {
			$list = $category->jobs()->get();
		} else {
			$list = \TNT\Job\Models\Job::all();
		}

	    $data = [];
	    foreach ($list as $item) {
		    $fieldData = [];
		    foreach ($item->fields_pivot_model as $field) {
			    $fieldData[$field->code] = $field->pivot->value;
		    }
		    $fieldData['url'] = $this->controller->pageUrl('job', ['id' => $item->id]);
		    $data[] = array_merge(
			    ['id' => $item->id],
			    $fieldData
		    );
	    }
	    $this->list = $this->page['list'] = $data;
    }

    function onSearchByCategory() {
    	$jobId = post('id');

	    $categoryRelation = \Db::table('tnt_job_option_entity')->where('entity_id', $jobId)->first();

	    $url = $this->controller->pageUrl('advance-search') . '?' . http_build_query(['category' => $categoryRelation->option_id]);
	    return \Redirect::to($url);
    }
}
