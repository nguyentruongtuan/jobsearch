<?php namespace Tnt\Job\Controllers;

use BackendMenu;
use Backend\Classes\Controller;
use TNT\Job\Classes\Provider;
use TNT\Job\Models\Settings;
use TNT\Job\Models\Field;
/**
 * Recruiter Back-end Controller
 */
class Recruiter extends Controller
{
    public $implement = [
        'Backend.Behaviors.FormController',
        'Backend.Behaviors.ListController',
        'Backend.Behaviors.RelationController',
    ];

    public $formConfig = 'config_form.yaml';
    public $listConfig = 'config_list.yaml';
    public $relationConfig = 'config_relation.yaml';

    public function __construct()
    {
        parent::__construct();

        BackendMenu::setContext('TNT.Job', 'job', 'recruiter');
    }

    public function onGetrecruiter() {
	    $provider = new Provider();
	    $partitionId = Settings::get('active_partition');

	    $maxId = \Db::table('tnt_job_recruiters')->max('id');
	    if (!$maxId) {
		    $maxId=0;
	    }
	    $query = [
		    "partition={$partitionId}",
		    "condition=Recruiter.P_Id:gt=" . $maxId,
		    'field=' . implode(',', \TNT\Job\Classes\Mapping\Recruiter::ATTRIBUTES)
	    ];
	    $url = $provider->endpoint . 'recruiter?' . implode('&', $query);

	    if (!$partitionId) {
		    Flash::error("Please active one partition");
		    return Redirect::refresh();
	    }
	    $request = $provider->getAuthenticatedRequest("GET", $url, $provider->token);
	    $response = $provider->getParsedResponse($request);
	    foreach ($response->getArray() as $data) {
		    $recruiter = new \TNT\Job\Models\Recruiter();
		    $recruiter->fill($data);
		    $recruiter->save();
	    }

	    \Flash::success('Done');
	    return \Redirect::refresh();
    }

    public function onGetfields() {

	    $provider = new Provider();
	    $partitionId = Settings::get('active_partition');
	    $query = [
		    "partition={$partitionId}",
		    'resource=9',
		    'count=200'
	    ];
	    $url = $provider->endpoint . 'field?' . implode('&', $query);

	    if (!$partitionId) {
		    Flash::error("Please active one partition");
		    return Redirect::refresh();
	    }

	    $request = $provider->getAuthenticatedRequest("GET", $url, $provider->token);
	    $response = $provider->getParsedResponse($request);
	    foreach ($response->getArray() as $data) {
		    $data['code'] = preg_replace('/[^a-z0-9]/', '_', strtolower($data['name']));
		    if (!Field::where('code', $data['code'])->first()) {
			    $r = new Field();
			    $r->fill(array_merge($data, ['resource_id' => '9']));
			    $r->save();
		    }
	    }

	    \Artisan::call('job:default-job-field');
    	\Flash::success('Done');
    }
}
