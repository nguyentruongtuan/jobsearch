<?php namespace TNT\Job\Models;

use Model;

/**
 * profile Model
 */
class Profile extends Model
{
    /**
     * @var string The database table used by the model.
     */
    public $table = 'tnt_job_profiles';


    /**
     * @var array Guarded fields
     */
    protected $guarded = ['*'];

    /**
     * @var array Fillable fields
     */
    protected $fillable = [
        'display_name',
        'gender',
        'dob',
        'salutation',
        'first_name',
        'last_name',
        'title',
        'mobile',
    ];

    /**
     * @var array Relations
     */
    public $hasOne = [];
    public $hasMany = [];
    public $belongsTo = [
        'user' => ['Rainlab\User\Models\User']
    ];
    public $belongsToMany = [];
    public $morphTo = [];
    public $morphOne = [];
    public $morphMany = [];
    public $attachOne = [];
    public $attachMany = [];
}
