<?php namespace TNT\Job\Console;

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;
use Log;
use TNT\Job\Classes\Provider;
use TNT\Job\Models\Field;
use TNT\Job\Models\Job;

class FieldDefault extends Command
{
    /**
     * @var string The console command name.
     */
    protected $name = 'job:default-job-field';

    /**
     * @var string The console command description.
     */
    protected $description = 'No description provided yet...';

    /**
     * Execute the console command.
     * @return void
     */
    public function fire()
    {
    	$fields = Field::where('required', true)->update(['active' => true]);

    }

    /**
     * Get the console command arguments.
     * @return array
     */
    protected function getArguments()
    {
        return [];
    }

    /**
     * Get the console command options.
     * @return array
     */
    protected function getOptions()
    {
        return [];
    }
}
