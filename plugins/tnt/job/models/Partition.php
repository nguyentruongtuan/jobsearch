<?php namespace TNT\Job\Models;

use Model;

/**
 * partition Model
 */
class Partition extends Model
{
    /**
     * @var string The database table used by the model.
     */
    public $table = 'tnt_job_partitions';

    /**
     * @var array Guarded fields
     */
    protected $guarded = ['*'];

    /**
     * @var array Fillable fields
     */
    protected $fillable = [
    	'api_id',
    	'name',
    	'company_id',
    ];

    /**
     * @var array Relations
     */
    public $hasOne = [];
    public $hasMany = [];
    public $belongsTo = [];
    public $belongsToMany = [];
    public $morphTo = [];
    public $morphOne = [];
    public $morphMany = [];
    public $attachOne = [];
    public $attachMany = [];
}
