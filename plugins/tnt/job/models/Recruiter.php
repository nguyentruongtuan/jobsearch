<?php namespace TNT\Job\Models;

use Model;

/**
 * recruiter Model
 */
class Recruiter extends Model
{
    /**
     * @var string The database table used by the model.
     */
    public $table = 'tnt_job_recruiters';

    /**
     * @var array Guarded fields
     */
    protected $guarded = ['*'];

    /**
     * @var array Fillable fields
     */
    protected $fillable = [
    	'id',
    	'recruiter_p_owner',
    	'recruiter_p_client',
    	'recruiter_p_registrationdate',
    	'recruiter_p_name',
    	'is_fetch',
    ];

    /**
     * @var array Relations
     */
    public $hasOne = [
        'entity' => ['TNT\Job\Models\Entity']
    ];
    public $hasMany = [
        'jobs' => ['TNT\Job\Models\Job']
    ];
    public $belongsTo = [
        'user' => ['Rainlab\User\Models\User']
    ];
    public $belongsToMany = [
	    'fields' => [
		    'TNT\Job\Models\Field',
		    'table' => 'tnt_job_field_values',
		    'conditions' => 'resource_type = 9',
		    'key' => 'resource_id',
	    ],
	    'fields_pivot_model' => [
		    'TNT\Job\Models\Field',
		    'table' => 'tnt_job_field_values',
		    'key' => 'resource_id',
		    'pivot' => ['value']
	    ]
    ];
    public $morphTo = [];
    public $morphOne = [];
    public $morphMany = [];
    public $attachOne = [];
    public $attachMany = [];
}
