<?php namespace Tnt\Job\Models;

use Model;

/**
 * related Model
 */
class Related extends Model
{
    /**
     * @var string The database table used by the model.
     */
    public $table = 'tnt_job_relateds';

    /**
     * @var array Guarded fields
     */
    protected $guarded = ['*'];

    /**
     * @var array Fillable fields
     */
    protected $fillable = [
    	'parent_id',
    	'child_id',
    ];

    /**
     * @var array Relations
     */
    public $hasOne = [];
    public $hasMany = [];
    public $belongsTo = [];
    public $belongsToMany = [];
    public $morphTo = [];
    public $morphOne = [];
    public $morphMany = [];
    public $attachOne = [];
    public $attachMany = [];
}
