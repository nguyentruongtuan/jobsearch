<?php namespace TNT\Keyword\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class CreateKeywordsTable extends Migration
{
    public function up()
    {
        Schema::create('tnt_keyword_keywords', function(Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('name');
            $table->string('slug')->nullable();
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('tnt_keyword_keywords');
    }
}
