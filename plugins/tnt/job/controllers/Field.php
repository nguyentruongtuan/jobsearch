<?php namespace TNT\Job\Controllers;

use BackendMenu;
use Backend\Classes\Controller;
use Db;
use Redirect;
use Flash;
/**
 * Field Back-end Controller
 */
class Field extends Controller
{
    public $implement = [
        'Backend.Behaviors.FormController',
        'Backend.Behaviors.ListController'
    ];

    public $formConfig = 'config_form.yaml';
    public $listConfig = 'config_list.yaml';

    public function __construct()
    {
        parent::__construct();

        BackendMenu::setContext('TNT.Job', 'job', 'field');
    }

    public function onClear() {
    	Db::table('tnt_job_fields')->truncate();
		return Redirect::refresh();
    }

    public function select() {
    	$id = $this->params[0];

    	$field = \TNT\Job\Models\Field::find($id);
    	$field->active = !$field->active;
    	$field->save();

    	Flash::success('Updated');
    	return Redirect::back();
    }

    public function update() {
    	return parent::update();
    }
}
