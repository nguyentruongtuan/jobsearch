<?php namespace Tnt\Job\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class CreateExpriencesTable extends Migration
{
    public function up()
    {
        Schema::create('tnt_job_expriences', function(Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->integer('user_id');
            $table->string('title');
            $table->string('type_of_work');
            $table->string('location');
            $table->string('from');
            $table->string('to');
            $table->string('description');
            $table->string('type');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('tnt_job_expriences');
    }
}
