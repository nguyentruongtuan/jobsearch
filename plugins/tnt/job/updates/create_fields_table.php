<?php namespace TNT\Job\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class CreateFieldsTable extends Migration
{
    public function up()
    {
        Schema::create('tnt_job_fields', function(Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->integer('id');
            $table->string('name');
            $table->string('code')->unique();
            $table->integer('type');
            $table->boolean('required');
            $table->string('max');
            $table->string('min');
            $table->string('decimal_fraction');
            $table->string('refer_to');
            $table->string('alias');
            $table->integer('resource_type');
            $table->boolean('active')->default(false);
            $table->timestamps();
            $table->primary('id');
        });
    }

    public function down()
    {
        Schema::dropIfExists('tnt_job_fields');
    }
}
