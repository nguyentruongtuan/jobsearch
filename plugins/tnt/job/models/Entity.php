<?php namespace TNT\Job\Models;

use Model;

/**
 * entity Model
 */
class Entity extends Model
{
    /**
     * @var string The database table used by the model.
     */
    public $table = 'tnt_job_entities';

    /**
     * @var array Guarded fields
     */
    protected $guarded = ['*'];

    /**
     * @var array Fillable fields
     */
    protected $fillable = [];

    /**
     * @var array Relations
     */
    public $hasOne = [];
    public $hasMany = [];
    public $belongsTo = [];
    public $belongsToMany = [
        'values' => ['TNT\Job\Models\Attribute', 'table' => 'tnt_job_attribute_strings']
    ];
    public $morphTo = [];
    public $morphOne = [];
    public $morphMany = [];
    public $attachOne = [];
    public $attachMany = [];
}
