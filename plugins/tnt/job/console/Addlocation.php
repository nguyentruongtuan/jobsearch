<?php namespace Tnt\Job\Console;

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;
use Tnt\Job\Models\Location;

class Addlocation extends Command
{
    /**
     * @var string The console command name.
     */
    protected $name = 'job:addlocation';

    /**
     * @var string The console command description.
     */
    protected $description = 'No description provided yet...';

    /**
     * Execute the console command.
     * @return void
     */
    public function fire()
    {

	    $options = simplexml_load_file(base_path() . '/option.xml');

	    $options = json_decode(json_encode($options));
	    $items = $options->{'Item'};
	    $categories = null;
	    for ($i = 0 ; $i < count($items) ; $i++) {
		    $item = $items[$i];
		    if ($item->{'Option.P_Id'} == 31) {
			    $categories = $item;
			    break;
		    }
	    }

	    $cats = [];

	    $cats[] = [
		    'id' => $categories->{'Option.P_Id'},
		    'name' => $categories->{'Option.P_Name'},
	    ];

	    $this->addItem($categories->Items->Item, $cats);

	    \Db::table('tnt_job_locations')->insert($cats);

	    $debugger = null;
    }

	function addItem($list, &$cats) {
		foreach ($list as $key => $value) {
			if (!is_object($value)) {
				continue;
			}

			if (count((array)$value) == 0) {
				continue;
			}
			$cats[] = [
				'id' => $value->{'Option.P_Id'},
				'name' => $value->{'Option.P_Name'},
			];


			if (isset($value->Items->Item)) {
				$this->addItem($value->Items->Item, $cats);
			}

		}
	}

    /**
     * Get the console command arguments.
     * @return array
     */
    protected function getArguments()
    {
        return [];
    }

    /**
     * Get the console command options.
     * @return array
     */
    protected function getOptions()
    {
        return [];
    }
}
