<?php namespace TNT\Job\Models;

use Model;

/**
 * job_jobcategory Model
 */
class JobJobcategory extends Model
{
    /**
     * @var string The database table used by the model.
     */
    public $table = 'tnt_job_job_jobcategories';

    /**
     * @var array Guarded fields
     */
    protected $guarded = ['*'];

    /**
     * @var array Fillable fields
     */
    protected $fillable = [];

    /**
     * @var array Relations
     */
    public $hasOne = [];
    public $hasMany = [];
    public $belongsTo = [];
    public $belongsToMany = [

    ];
    public $morphTo = [];
    public $morphOne = [];
    public $morphMany = [];
    public $attachOne = [];
    public $attachMany = [];
}
