<?php

namespace TNT\Job\Classes;

use League\OAuth2\Client\Grant\AuthorizationCode;

class AuthrorizationCode extends AuthorizationCode {

	protected function getName() {
		return 'oauth_code';
	}

	/**
	 * Prepares an access token request's parameters by checking that all
	 * required parameters are set, then merging with any given defaults.
	 *
	 * @param  array $defaults
	 * @param  array $options
	 * @return array
	 */
	public function prepareRequestParameters(array $defaults, array $options)
	{
		$defaults['response_type'] = $this->getName();

		return parent::prepareRequestParameters($defaults, $options);
	}
}