<?php namespace TNT\Job\Classes\Mapping;

class Recruiter extends BaseMapping {

	const ATTRIBUTES = [
		'id' => 'Recruiter.P_Id',
		'recruiter_p_owner' => 'Recruiter.P_Owner',
		'recruiter_p_client' => 'Recruiter.P_Client',
		'recruiter_p_registrationdate' => 'Recruiter.P_RegistrationDate',
		'recruiter_p_name' => 'Recruiter.P_Name',
	];

	public function getAttributes() {
		return self::ATTRIBUTES;
	}

}