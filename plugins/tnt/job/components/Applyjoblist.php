<?php namespace Tnt\Job\Components;

use Cms\Classes\ComponentBase;

class Applyjoblist extends ComponentBase
{
	public $list;
    public function componentDetails()
    {
        return [
            'name'        => 'applyjoblist Component',
            'description' => 'No description provided yet...'
        ];
    }

    public function defineProperties()
    {
        return [];
    }

    public function onRun() {
	    $user = \Auth::getUser();
	    if ($user) {
	    	$jobs = $user->job_applied()->get();
	    	$this->list = $this->page['list'] = $jobs;
	    }
    }
}
