<?php namespace TNT\Job\Controllers;

use BackendMenu;
use Backend\Classes\Controller;
use TNT\Job\Classes\Provider;
use TNT\Job\Models\Settings;
use Session;
use Flash;
use Redirect;
use TNT\Job\Models\Partition as PModel;
/**
 * Partition Back-end Controller
 */
class Partition extends Controller
{
    public $implement = [
        'Backend.Behaviors.FormController',
        'Backend.Behaviors.ListController'
    ];

    public $formConfig = 'config_form.yaml';
    public $listConfig = 'config_list.yaml';

	public $requiredPermissions = ['tnt.job.oauth'];

    public function __construct()
    {
        parent::__construct();

        BackendMenu::setContext('TNT.Job', 'job', 'partition');
    }

	public function onConnect() {

		$provider = new Provider([
			'clientId'                => Settings::get('client_id'),    // The client ID assigned to you by the provider
			'clientSecret'            => Settings::get('client_secret'),   // The client password assigned to you by the provider
			'redirectUri'             => url('/tnt/callback'),
			'urlAuthorize'            => Settings::get('authorize_url'),
			'urlAccessToken'          => Settings::get('token_url'),
			'urlResourceOwnerDetails' => Settings::get('owner_url'),
			'scopes' => [
				'user_r',
				'user_w',
				'partition_r',
				'partition_w',
				'field_r',
				'field_w',
				'option_r',
				'option_w',
				'client_r',
				'client_w',
				'recruiter_r',
				'recruiter_w',
				'job_r',
				'job_w',
				'candidate_r',
				'candidate_w',
				'resume_r',
				'resume_w',
				'process_r',
				'process_w',
				'attachment_r',
				'attachment_w',
				'activity_r',
				'activity_w',
				'sales_r',
				'sales_w',
			],
		]);

		// call authorize url for generating state
		$url = $provider->getAuthorizationUrl();

		// put state to session for redirect validate
		Session::put('state', $provider->getState());


		// request to authorize url for code
		return Redirect::to($url);
	}

	public function redirect() {
		$state = Request::Input('state');
		if (!$state || $state !== Session::get('authorize-state')) {
			return 'Invalid state';
		}

		$code = Request::Input('code');
		if (!$code) {
			return 'Invalid code';
		}
	}

	public function onGetPartition() {
		$provider = new Provider();
		$url = $provider->endpoint . 'partition';
		$request = $provider->getAuthenticatedRequest('GET', $url, $provider->token);
		$response = $provider->getParsedResponse($request);

		foreach ($response->getArray() as $item) {

			if (!PModel::where('api_id', $item['api_id'])->first()) {
				$p = new PModel();
				$p->fill($item);
				$p->save();
			}
		}

		Flash::success("Partition update");
		return Redirect::refresh();
	}

	public function active() {
    	$id = $this->params[0];
    	Settings::set('active_partition', $id);
    	return Redirect::to('/backend/tnt/job/partition');
	}
}
