<?php namespace TNT\Job\Console;

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;
use TNT\Job\Classes\Provider;
use TNT\Job\Models\Field;
use TNT\Job\Models\Job;
use Tnt\Job\Models\Location;
use TNT\Job\Models\Settings;

class Fetch extends Command
{
    /**
     * @var string The console command name.
     */
    protected $name = 'job:fetch';

    /**
     * @var string The console command description.
     */
    protected $description = 'No description provided yet...';

    /**
     * Execute the console command.
     * @return void
     */
    public function fire()
    {
        $provider = new Provider();
        $partition = Settings::get('active_partition');

        $activeFields = Field::query()->select('*')->where('active', true)->where('resource_type', 3);

        $fieldArr = $activeFields->lists('alias');
	    $fieldArr[] = 'Job.P_Id';

	    for ($i =0 ; $i < 3; $i++) {
		    $ids = [];
		    $jobs = Job::where('fetch', false)->take(10)->get();
		    foreach ($jobs as $jobItem) {
			    $ids[] = $jobItem->id;
		    }

		    if (!count($ids)) {
			    $this->fixUnfetch();
			    return;
		    }

		    $query = [
			    'partition=' . $partition,
			    'condition=Job.P_Id:or=' . implode(':', $ids),
			    'field=' . implode(',', $fieldArr)
		    ];
		    $url = $provider->endpoint . 'job?' . implode('&', $query);
		    $request = $provider->getAuthenticatedRequest('GET', $url, $provider->token);
		    $response = $provider->getResponse($request);

		    $body = $response->getBody()->getContents();
		    $body = simplexml_load_string($body);
		    $body = \GuzzleHttp\json_decode(\GuzzleHttp\json_encode($body));

		    if (in_array($body->Code, ['401'])) {
			    echo 'Cannot get data Code:' . $body->Code;
			    return;
		    }

		    if (!$body->Item) {
			    return;
		    }

		    if (!is_array($body->Item)) {
			    $body->Item = [$body->Item];
		    }

		    foreach ($body->Item as $apiItem) {
			    $job = $jobs->where('id', (int)$apiItem->{'Job.P_Id'})->first();
			    if (!$job) {
				    continue;
			    }

			    $syncData = [];
				$jobCategory = null;
				$jobLocation = null;
				$jobNationality = null;
			    foreach ($activeFields->get() as $field) {
				    $value = $apiItem->{$field->alias};
				    if ($field->alias == 'Job.P_JobCategory') {
				    	 $child = reset($value);
				    	 $jobCategory = $child->{'Option.P_Id'};
				    }

				    if ($field->alias == 'Job.P_Area') {
					    $child = reset($value);
					    $jobLocation = $child->{'Option.P_Id'};
				    }

				    if ($field->alias == 'Job.U_5A291587EABD81A40B379C69125FDB') {
					    $child = reset($value);
					    $jobNationality = $child->{'Option.P_Id'};
				    }

				    if (is_object($value)) {
					    for ($i=0 ; $i<5 ; $i++) {
						    $value = reset($value);
						    if (!is_object($value)) {
							    break;
						    }
					    }
				    }
				    $syncData[$field->id] = [
					    'value' => $value
				    ];
			    }
			    $job->fields()->sync($syncData);
			    if ($jobCategory) {
			    	$job->jobcategories()->attach($jobCategory);
			    }

			    if ($jobLocation) {
				    $job->location()->attach($jobLocation);
			    }

			    if ($jobNationality) {
				    $job->nationality()->attach($jobNationality);
			    }
		    }

		    \Db::table('tnt_job_jobs')->whereIn('id', $jobs->lists('id'))->update(['fetch' => true]);
	    }

	    $this->fixUnfetch();

    }

    public function fixUnfetch() {
	    // fix cannot fetch data issue, but mark records as fetched
	    $unfetchedIds = \Db::table('tnt_job_field_values')
	                       ->join('tnt_job_fields', 'tnt_job_field_values.field_id', '=', 'tnt_job_fields.id')
	                       ->select('tnt_job_field_values.resource_id')
	                       ->where('tnt_job_fields.resource_type', 3)
	                       ->groupBy('tnt_job_field_values.resource_id')
	                       ->havingRaw('COUNT(*) <= 13')
	                       ->lists('tnt_job_field_values.resource_id');

	    \Db::table('tnt_job_jobs')->whereIn('id', $unfetchedIds)->update(['fetch' => false]);
    }

    /**
     * Get the console command arguments.
     * @return array
     */
    protected function getArguments()
    {
        return [];
    }

    /**
     * Get the console command options.
     * @return array
     */
    protected function getOptions()
    {
        return [];
    }
}
