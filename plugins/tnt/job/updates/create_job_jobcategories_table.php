<?php namespace TNT\Job\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class CreateJobJobcategoriesTable extends Migration
{
    public function up()
    {
        Schema::create('tnt_job_job_jobcategories', function(Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->integer('job_id');
            $table->integer('jobcategory_id');
            $table->primary(['job_id', 'jobcategory_id']);
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('tnt_job_job_jobcategories');
    }
}
