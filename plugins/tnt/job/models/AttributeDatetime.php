<?php namespace TNT\Job\Models;

use Model;

/**
 * attribute_datetime Model
 */
class AttributeDatetime extends Model
{
    /**
     * @var string The database table used by the model.
     */
    public $table = 'tnt_job_attribute_datetimes';

    /**
     * @var array Guarded fields
     */
    protected $guarded = ['*'];

    /**
     * @var array Fillable fields
     */
    protected $fillable = [];

    /**
     * @var array Relations
     */
    public $hasOne = [];
    public $hasMany = [];
    public $belongsTo = [];
    public $belongsToMany = [];
    public $morphTo = [];
    public $morphOne = [];
    public $morphMany = [];
    public $attachOne = [];
    public $attachMany = [];
}
