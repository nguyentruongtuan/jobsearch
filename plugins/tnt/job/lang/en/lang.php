<?php

return [
    'plugin' => [
        'tab' => 'Job',
        'tab_oauth' => 'API OAuth',
    ],
    'settings' => [
        'oauth_tab' => 'OAuth',
        'menu_label' => 'Job search',
        'menu_description' => 'Job search settings'
    ],
    'recruiter' => [
        'name' => 'Recruiter',
        'description' => 'Recruiter user group'
    ],
    'candidate' => [
        'name' => 'Candidate',
        'description' => 'Candidate user group'
    ]
];