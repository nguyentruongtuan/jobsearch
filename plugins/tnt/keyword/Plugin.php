<?php namespace TNT\Keyword;

use Backend;
use System\Classes\PluginBase;
use TNT\Job\Models\Job;

/**
 * keyword Plugin Information File
 */
class Plugin extends PluginBase
{
	public $require = ['TNT.Job'];
    /**
     * Returns information about this plugin.
     *
     * @return array
     */
    public function pluginDetails()
    {
        return [
            'name'        => 'keyword',
            'description' => 'Add keyword function for job plugin',
            'author'      => 'TNT',
            'icon'        => 'icon-leaf'
        ];
    }

    /**
     * Register method, called when the plugin is first registered.
     *
     * @return void
     */
    public function register()
    {

    }

    /**
     * Boot method, called right before the request route.
     *
     * @return array
     */
    public function boot()
    {
		Job::extend(function ($model) {
			$model->belongsToMany['keywords'] = [
				'TNT\Keyword\Models\Keyword',
				'table' => 'tnt_keyword_keyword_jobs'
			];
		});
    }

    /**
     * Registers any front-end components implemented in this plugin.
     *
     * @return array
     */
    public function registerComponents()
    {
        return []; // Remove this line to activate

        return [
            'TNT\Keyword\Components\MyComponent' => 'myComponent',
        ];
    }

    /**
     * Registers any back-end permissions used by this plugin.
     *
     * @return array
     */
    public function registerPermissions()
    {
        return []; // Remove this line to activate

        return [
            'tnt.keyword.some_permission' => [
                'tab' => 'keyword',
                'label' => 'Some permission'
            ],
        ];
    }

    /**
     * Registers back-end navigation items for this plugin.
     *
     * @return array
     */
    public function registerNavigation()
    {
        return []; // Remove this line to activate

        return [
            'keyword' => [
                'label'       => 'keyword',
                'url'         => Backend::url('tnt/keyword/mycontroller'),
                'icon'        => 'icon-leaf',
                'permissions' => ['tnt.keyword.*'],
                'order'       => 500,
            ],
        ];
    }
}
