<?php namespace Tnt\Job\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class CreateBookmarksTable extends Migration
{
    public function up()
    {
        Schema::create('tnt_job_bookmarks', function(Blueprint $table) {
            $table->engine = 'InnoDB';
	        $table->integer('user_id');
	        $table->integer('resource_id');
            $table->timestamps();
            $table->primary(['user_id', 'resource_id']);
        });
    }

    public function down()
    {
        Schema::dropIfExists('tnt_job_bookmarks');
    }
}
