<?php namespace TNT\Job\Models;

use Model;

/**
 * job Model
 */
class Job extends Model
{
    /**
     * @var string The database table used by the model.
     */
    public $table = 'tnt_job_jobs';

    /**
     * @var array Guarded fields
     */
    protected $guarded = ['*'];

    /**
     * @var array Fillable fields
     */
    protected $fillable = [
    	'id',
    	'owner_id',
    	'name',
    	'recruiter_id',
    	'registration_date',
    	'registered_by',
    	'update_date',
    	'updated_by',
    	'min_salary',
    	'max_salary',
    	'fetch',
    ];

    /**
     * @var array Relations
     */
    public $hasOne = [
        'entity' => ['TNT\Job\Models\Entity']
    ];
    public $hasMany = [];
    public $belongsTo = [
        'recruiter' => ['TNT\Job\Models\Recruiter']
    ];
    public $belongsToMany = [
        'categories' => ['TNT\Job\Models\Jobcategory', 'table' => 'tnt_job_job_jobcategories'],
        'related' => [
	        'TNT\Job\Models\Job',
	        'table' => 'tnt_job_relateds',
	        'key' => 'parent_id',
	        'otherKey' => 'child_id',
        ],
        'fields' => [
        	'TNT\Job\Models\Field',
	        'table' => 'tnt_job_field_values',
	        'conditions' => 'resource_type = 3',
	        'key' => 'resource_id',
        ],
        'fields_pivot_model' => [
	        'TNT\Job\Models\Field',
	        'table' => 'tnt_job_field_values',
	        'key' => 'resource_id',
	        'pivot' => ['value']
        ],
        'search_data' => [
	        'TNT\Job\Models\Field',
	        'table' => 'tnt_job_field_values',
	        'key' => 'resource_id',
	        'pivot' => ['value', 'code']
        ],
	    'users' => [
		    'TNT\Job\Models\User',
		    'table' => 'tnt_job_bookmarks',
		    'key' => 'resource_id'
	    ],
	    'user_applied' => [
		    'Rainlab\User\Models\User',
		    'table' => 'tnt_job_userjobs',
		    'conditions' => 'is_activated = 1'
	    ]
    ];
    public $morphTo = [

    ];
    public $morphOne = [

    ];
    public $morphMany = [

    ];
	public $morphToMany = [
	    'jobcategories' => [
	    	'TNT\Job\Models\Option',
		    'name' => 'entity',
		    'table' => 'tnt_job_option_entity',
		    'conditions' => 'type = 29'
	    ],
	    'location' => [
		    'TNT\Job\Models\Option',
		    'name' => 'locationable',
		    'table' => 'tnt_job_location_entity',
		    'conditions' => 'type = 31',
		    'select' => 'name'
	    ],
	    'nationality' => [
		    'TNT\Job\Models\Option',
		    'name' => 'nationable',
		    'table' => 'tnt_job_nationality_entity',
		    'conditions' => 'type = 10056',
		    'select' => 'name'
	    ],

    ];
    public $attachOne = [];
    public $attachMany = [];

    public function scopeIsPosition($query) {
		return $query;
    }

    public function getDetailUrl() {
	    return url('job', $this->getKey());
    }

    public function value($key) {

    }

    public function isBookmarked() {
    	if (!$user = \Auth::getUser()) {
    		return false;
	    }

	    return $user->bookmarks()->where('id', $this->getKey())->first();
    }
}
