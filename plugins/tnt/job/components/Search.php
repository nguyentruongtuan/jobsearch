<?php namespace Tnt\Job\Components;

use Cms\Classes\ComponentBase;
use TNT\Job\Models\Jobcategory;
use Tnt\Job\Models\Location;
use Tnt\Job\Models\Option;

class Search extends ComponentBase
{

	public $result;

    public function componentDetails()
    {
        return [
            'name'        => 'search Component',
            'description' => 'No description provided yet...'
        ];
    }

    public function defineProperties()
    {
        return [];
    }

    public function onSearchRedirect() {

	    $params = http_build_query(post());

    	$url = $this->controller->pageUrl('advance-search') . "?$params";

    	return \Redirect::to($url);
    }

    public function onSearch() {

	    $url = $this->controller->pageUrl('advance-search', [
		    'title' => post('title'),
		    'category' => post('category'),
		    'location' => post('location'),
	    ]);

	    return \Redirect::to($url);

    }

    public function queryJob($title, $category, $location) {

	    // query job and redirect to result page

	    $query = \Db::table('tnt_job_field_values')
	                ->select('resource_id')
	                ->join('tnt_job_fields', 'tnt_job_field_values.field_id', '=', 'tnt_job_fields.id');
	    $query->where('tnt_job_fields.resource_type', 3);

	    $result = [];

	    // title is position in database
	    if (!!$title) {
	    	$itemQuery = $query;
		    $itemQuery->where('tnt_job_fields.code', 'position');
		    $itemQuery->where('tnt_job_field_values.value', 'like', "%$title%");
			$result = array_merge($result, $itemQuery->lists('resource_id'));
	    }

	    if (!!$category) {
	    	$jobCategory = \Db::table('tnt_job_option_entity')->where('option_id', $category)->lists('entity_id');

	    	if (count($result) == 0) {
	    		$result = $jobCategory;
		    } else {
			    $result = array_filter($result, function($v) use ($jobCategory) {
				    return in_array($v, $jobCategory);
			    });
		    }

	    }

	    if (!!$location) {
		    $jobLocation = \Db::table('tnt_job_location_entity')->where('option_id', $location)->lists('locationable_id');

		    if (count($result) == 0) {
		    	$result = $jobLocation;
		    } else {
			    $result = array_filter($result, function($v) use ($jobLocation) {
				    return in_array($v, $jobLocation);
			    });
		    }

	    }

	    if (!$title && !$category && !$location) {
	    	// add call jobs to list 
	    	$result = $query->lists('resource_id');
	    }


	    return \TNT\Job\Models\Job::whereIn('id', $result);
    }

    public function onRun() {
	    $this->addJs('/plugins/tnt/job/assets/js/search.js');

	    $title = \Input::get('title');
	    $category = \Input::get('category');
	    $location = \Input::get('location');

	    $category = preg_replace('/[()\/]/', ' ', $category);

	    if ($title == 'default') $title = null;
	    if ($category == 'default') $category = 0;
	    if ($location == 'default') $location = 0;


	    $this->page['title'] = $title;
	    $this->page['jobcategory'] = $category;
	    $this->page['joblocation'] = $location;

	    // get job categories from options table
	    $categoryList = \Db::table('tnt_job_option_entity')->lists('option_id');
	    $this->page['category_list'] = Option::whereIn('id', array_unique($categoryList))->get();

	    $locationList = \Db::table('tnt_job_location_entity')->lists('option_id');
	    $this->page['location_list'] = Option::whereIn('id', array_unique($locationList))->get();

	    $nationList = \Db::table('tnt_job_nationality_entity')->lists('option_id');
	    $this->page['nation_list'] = Option::whereIn('id', array_unique($nationList))->get();

	    $result = $this->queryJob($title, $category, $location);

	    // apply filter
	    $filterLocation = \Request::query('filter_location');
	    if (!!$filterLocation) {
	    	$filterLocation = explode(',', $filterLocation);
			$allowJobs = \Db::table('tnt_job_location_entity')->whereIn('option_id', $filterLocation)->lists('locationable_id');
			$allowJobs = array_unique($allowJobs);
			$result->whereIn('id', $allowJobs);
			$this->page['filter_location'] = $filterLocation;
	    }

	    $filterCategory = \Request::query('filter_category');
	    if (!!$filterCategory) {
		    $filterCategory = explode(',', $filterCategory);
		    $allowJobs = \Db::table('tnt_job_option_entity')->whereIn('option_id', $filterCategory)->lists('entity_id');
		    $allowJobs = array_unique($allowJobs);
		    $result->whereIn('id', $allowJobs);
		    $this->page['filter_category'] = $filterCategory;
	    }

	    $filterSalary = \Request::query('filter_salary');
	    if (!!$filterSalary) {
		    $filterSalary = explode(',', $filterSalary);
		    $values = explode('-', $filterSalary[0]);

		    $result->where('min_salary', '>=', $values[0]);
		    $result->where('max_salary', '<=', $values[1]);
		    $this->page['filter_salary'] = implode('-', $filterSalary);
	    }

	    $filteNation = \Request::query('filter_nation');
	    if (!!$filteNation) {
		    $filteNation = explode(',', $filteNation);
		    $allowJobs = \Db::table('tnt_job_nationality_entity')->whereIn('option_id', $filteNation)->lists('nationable_id');
		    $allowJobs = array_unique($allowJobs);
		    $result->whereIn('id', $allowJobs);
		    $this->page['filter_nation'] = $filteNation;
	    }

	    $orderBy = \Input::get('order');
	    $orderByType = \Request::query('order-type');
	    if (!$orderBy) {
	    	$orderBy = 'min_salary';
	    }

	    if (!$orderByType) {
		    $orderByType = 'asc';
	    }

	    $result->orderBy($orderBy, $orderByType);



	    $this->page['active_sort'] = $orderBy;
	    $this->page['total'] = $result->count();
	    $this->result = $result->paginate(10);
    }

    function getPagination() {

    }

    public function onAddFilter() {
    	$currentQuery = \Request::query();
    	foreach (['filter_location', 'filter_category', 'filter_nation'] as $filterType) {
		    if (key_exists($filterType, $currentQuery)) {
			    $currentQueryLocation = explode(',', $currentQuery[$filterType]);
			    $postFilter =  post($filterType);

			    // remove if exists
			    foreach ($currentQueryLocation as $key => $value) {
				    if ($value == $postFilter[0]) {
					    $currentQueryLocation[$key] = null;
					    $postFilter = [];
					    break;
				    }
			    }
			    $currentQueryLocation = array_filter($currentQueryLocation);
			    if (!$postFilter) {
				    $postFilter = [];
			    }
			    $currentQuery[$filterType] = implode(',', array_merge($currentQueryLocation, $postFilter));

		    } else {
			    $postFilter = post($filterType);
			    if ($postFilter) {
				    $currentQuery[$filterType] = implode(',', $postFilter);
			    }

		    }
	    }

	    $filterSalary = post('filter_salary');
	    if ($filterSalary) {
		    $currentQuery['filter_salary'] = $filterSalary;
	    } else {
		    $currentQuery['filter_salary'] = null;
	    }

	    $currentQuery = array_filter($currentQuery);

	    // build new url
	    $queryString = http_build_query($currentQuery);

	    $url = $this->controller->pageUrl('advance-search') . "?$queryString";
		return \Redirect::to($url);
    }

    public function onBookmark() {
    	$jobId = post('jobId');
	    $isBookmarked = !!post('isBookmarked');
    	$user = \Auth::getUser();

    	if ($user) {
    		$user->bookmarks()->detach($jobId);
		    if (!$isBookmarked) {
			    $user->bookmarks()->attach($jobId);
		    }

		    $url = $this->controller->pageUrl('advance-search') . "?" . http_build_query(\Request::query());
		    return \Redirect::to($url);
	    }
    }

    public function onSort() {
    	$query = \Request::query();

    	$orderBy = post('orderBy');

    	if ($orderBy) {
		    $query['order'] = $orderBy;
		    $type = \Input::get('order-type');
		    $type = $type =='desc' ? 'asc' : 'desc';
		    $query['order-type'] = $type;

	    }

	    $url = $this->controller->pageUrl('advance-search') . "?" . http_build_query($query);
		return \Redirect::to($url);
    }
}
