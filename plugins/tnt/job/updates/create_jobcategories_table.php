<?php namespace TNT\Job\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class CreateJobcategoriesTable extends Migration
{
    public function up()
    {
        Schema::create('tnt_job_jobcategories', function(Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('name');
            $table->string('title');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('tnt_job_jobcategories');
    }
}
