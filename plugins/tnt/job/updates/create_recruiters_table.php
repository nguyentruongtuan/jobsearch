<?php namespace TNT\Job\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class CreateRecruitersTable extends Migration
{
    public function up()
    {
        Schema::create('tnt_job_recruiters', function(Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->integer('id');
            $table->integer('recruiter_p_owner');
            $table->integer('recruiter_p_client')->nullable();
            $table->string('recruiter_p_registrationdate')->nullable();
            $table->string('recruiter_p_name');
            $table->boolean('is_fetch')->default(false);
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('tnt_job_recruiters');
    }
}
