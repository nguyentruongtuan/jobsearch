$(document).ready(function() {
	var modals = [];
	var currentStep = 0;
	var requestFunctions = ['onApply', 'onSaveInfo', 'onSaveExperience', 'onSaveExperience'];

	$('.apply-modal').each(function(index, item) {
		var modal = $(item);


        function next() {
            modals[currentStep].close();
            currentStep++;
            modals[currentStep].open();
        }

		modal.close = function() {
			this.removeClass('showing');
		};

		modal.open = function() {
			this.addClass('showing');
		};

		modal.find('.remove').click(function() {
			this.close();
		}.bind(modal));

		modal.find('.btn-cancel').click(function() {
			this.close();
		}.bind(modal));

		modal.find('.btn-upload').click(function () {
			$(this).prev().click();
        });

        modal.find('.btn-fill').click(function () {
            next();
        }.bind(modal));

        modal.onApply = function (data) {
			// apply success at first popup, reidrect
			location.href = '/apply-success';
        };

        modal.onSaveExperience = function (data) {
        	if (index !== requestFunctions.length-1) {
                return next();
			}

			currentStep = 0;
			modals[0].submitApply();
        };

        modal.onSaveInfo = function (data) {
			next();
        };

        modal.find('.experience.add').click(function () {
            $(this).parent().hide();
            modal.find('.add-new-exp').show();
        });


		modal.find('.modal-footer .btn-save').click(function() {
			this.submitApply();
		}.bind(modal));

		modal.submitApply = function () {
            // this.close();
            var $form = this.find('form');
            $form.request(requestFunctions[currentStep], {
                flash: true,
                files: true,
                redirect: '/apply-success',
                success: function (data) {
                    this[requestFunctions[currentStep]](data);

                }.bind(this),
                error: function (error) {
                    var errorObj = JSON.parse(error.responseText);
                    $.oc.flashMsg({text: errorObj['X_OCTOBER_ERROR_MESSAGE'], class: 'error'});
                    modal.find('.error-message').show();
                }
            });
        };

		modal.find('.upload-cv').click(function () {

			fileControl.click();
        });

        var fileControl = $('.upload-cv-file');
        fileControl.one('change', function () {
            fileControl.parent().submit();
        });

		modals.push(modal);

	});

	$('.btn-apply').click(function() {
		modals[currentStep].open();
	});

	$('.x2.CV-item').each(function (index, item) {
		$(item).click(function () {
            $('.x2.CV-item').removeClass('active');
            $(item).addClass('active');
        });
    })

});