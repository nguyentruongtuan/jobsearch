<?php namespace Tnt\Job\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class CreateRelatedsTable extends Migration
{
    public function up()
    {
        Schema::create('tnt_job_relateds', function(Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->integer('parent_id')->unsigned();
            $table->integer('child_id')->unsigned();
            $table->primary(['parent_id', 'child_id']);
        });
    }

    public function down()
    {
        Schema::dropIfExists('tnt_job_relateds');
    }
}
