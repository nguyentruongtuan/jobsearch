<?php namespace TNT\Job\Classes\Mapping;

use GuzzleHttp\Psr7\Response;

interface Mapping {

	function __construct(Response $data);

	/**
	 * @return array
	 */
	public function getArray();
}