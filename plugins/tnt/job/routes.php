<?php

use TNT\Job\Classes\Provider;
use TNT\Job\Classes\AuthrorizationCode as Grant;
use TNT\Job\Models\Settings;
Route::get('/tnt/callback', function () {

	if (Request::Input('state') != Session::get('state')) {
		// @TODO: remove state from sesssion
		return Response::make('Invalid state');
	}

	if (!$code = Request::Input('code')) {
		return Response::make('Invalid code');
	}

	$provider = new Provider([
		'clientId'                => Settings::get('client_id'),    // The client ID assigned to you by the provider
		'clientSecret'            => Settings::get('client_secret'),   // The client password assigned to you by the provider
		'redirectUri'             => url('/tnt/callback'),
		'urlAuthorize'            => Settings::get('authorize_url'),
		'urlAccessToken'          => Settings::get('token_url'),
		'urlResourceOwnerDetails' => Settings::get('owner_url'),
		'scopes' => [
			'recruiter_r',
			'partition_r',
			'client_r',
			'user_r',
			'option_r',
		],
	]);
	$provider->getGrantFactory()->setGrant('authorization_code', new Grant());
	$token = $provider->getAccessToken('authorization_code', ['code' => $code]);

	// store code in database
	Settings::set('access_token', $token->getToken());
	Settings::set('refresh_token', $token->getRefreshToken());
	Settings::set('expires', $token->getExpires());

//	$request = $provider->getAuthenticatedRequest('GET', 'https://api-hrbc-jp.porterscloud.com/v1/partition', $token);
//	$response = $provider->getResponse($request);



	return Redirect::to(url('/backend/tnt/job/partition'));
});