<?php namespace TNT\Job\Controllers;

use BackendMenu;
use Backend\Classes\Controller;
use Doctrine\DBAL\Query\QueryException;
use League\Flysystem\Exception;
use TNT\Job\Classes\Provider;
use TNT\Job\Models\Settings;
use TNT\Job\Models\Field as ApiField;
use Redirect;
use Db;
use Flash;
use \TNT\Job\Models\Field;
/**
 * Job Back-end Controller
 */
class Job extends Controller
{
    public $implement = [
        'Backend.Behaviors.FormController',
        'Backend.Behaviors.ListController',
	    'Backend.Behaviors.RelationController',
    ];

    public $formConfig = 'config_form.yaml';
    public $listConfig = 'config_list.yaml';
	public $relationConfig = 'config_relation.yaml';
//    public $bodyClass = 'compact-container';

    public function __construct()
    {
        parent::__construct();

        BackendMenu::setContext('TNT.Job', 'job', 'job');
    }

    public function onGetJob () {
		$provider = new Provider();
	    $partitionId = Settings::get('active_partition');

	    $maxId = Db::table('tnt_job_jobs')->max('id');
	    if (!$maxId) {
	    	$maxId=0;
	    }
	    $query = [
		    "partition={$partitionId}",
		    "condition=Job.P_Id:gt=" . $maxId,
		    'field=' . implode(',', \TNT\Job\Classes\Mapping\Job::ATTRIBUTES)
	    ];
		$url = $provider->endpoint . 'job?' . implode('&', $query);

	    if (!$partitionId) {
	    	Flash::error("Please active one partition");
	    	return Redirect::refresh();
	    }
		$request = $provider->getAuthenticatedRequest("GET", $url, $provider->token);
		$response = $provider->getParsedResponse($request);
		foreach ($response->getArray() as $data) {
			$job = new \TNT\Job\Models\Job();
			$job->fill($data);
			$job->save();
		}

		Flash::success('Done');
		return Redirect::refresh();
    }

    public function onFetchField() {
	    $provider = new Provider();
	    $partitionId = Settings::get('active_partition');
	    $query = [
		    "partition={$partitionId}",
		    'resource=3',
		    'count=200'
	    ];
	    $url = $provider->endpoint . 'field?' . implode('&', $query);

	    if (!$partitionId) {
		    Flash::error("Please active one partition");
		    return Redirect::refresh();
	    }

	    $request = $provider->getAuthenticatedRequest("GET", $url, $provider->token);
	    $response = $provider->getParsedResponse($request);
	    foreach ($response->getArray() as $data) {
	    	$data['code'] = preg_replace('/[^a-z0-9]/', '_', strtolower($data['name']));
	    	if (!Field::where('code', $data['code'])->first()) {
			    $r = new Field();
			    $r->fill($data);
			    $r->save();
		    }
	    }

	    \Artisan::call('job:default-job-field');

	    Flash::success("Done");
	    return Redirect::refresh();
    }


    public function formExtendFields($form) {

		return;
		// update attributes value from API
	    $provider = new Provider();
	    $partitionId = Settings::get('active_partition');

	    $fields = Db::table('tnt_job_fields')->select('*')->where('resource_type', 3)->where('active', true);
	    $fieldsArr = $fields->lists('alias');
	    $query = [
		    "partition={$partitionId}",
		    'field=' . implode(',', $fieldsArr),
		    'condition=Job.P_Id:eq=' . $form->model->id
	    ];
	    $url = $provider->endpoint . 'job?' . implode('&', $query);

	    $request = $provider->getAuthenticatedRequest('GET', $url, $provider->token);
	    $response = $provider->getResponse($request);

	    $body = $response->getBody()->getContents();
	    $body = simplexml_load_string($body);
	    $body = \GuzzleHttp\json_decode(\GuzzleHttp\json_encode($body));

	    if (!$body->Item) {
	    	return;
	    }

	    $extend = [];
	    foreach ($fields->get() as $field) {
	    	$fieldModel = $form->model->fields->find($field->id);
	    	if ($fieldModel) {
				$val = $this->getValue($body->Item->{$field->alias});
	    		$fieldModel->pivot->value = $val;
			    $fieldModel->pivot->save();
		    } else {
	    		$value = $this->getValue($body->Item->{$field->alias});
	    		$fieldModel = $form->model->fields()->attach($field->id, [
				    'value' => $value
			    ]);
		    }

	    }

    }

    protected function getValue($value) {
	    for ($i=0; $i<5 ; $i++) {
		    if (is_object($value)) {
			    $value = reset($value);
		    } else {
			    break;
		    }
	    }

	    return $value;
    }

    public function onRefreshData() {
    	$id = $this->params[0];
	    $provider = new Provider();
	    $partition = Settings::get('active_partition');

	    $activeFields = Field::query()->select('*')->where('active', true)->where('resource_type', 3);
	    $fieldArr = $activeFields->lists('alias');
	    $fieldArr[] = 'Job.P_Id';

	    $query = [
		    'partition=' . $partition,
		    'condition=Job.P_Id=' . $id,
		    'field=' . implode(',', $fieldArr)
	    ];
	    $url = $provider->endpoint . 'job?' . implode('&', $query);
	    $request = $provider->getAuthenticatedRequest('GET', $url, $provider->token);
	    $response = $provider->getResponse($request);

	    $body = $response->getBody()->getContents();
	    $body = simplexml_load_string($body);
	    $body = \GuzzleHttp\json_decode(\GuzzleHttp\json_encode($body));

	    if (in_array($body->Code, ['401'])) {
		    echo 'Cannot get data Code:' . $body->Code;
		    return;
	    }

	    if (!$body->Item) {
		    return;
	    }

		$job = \TNT\Job\Models\Job::find($id);

	    $syncData = [];
	    foreach ($activeFields->get() as $field) {
		    $value = $body->Item->{$field->alias};
		    if (is_object($value)) {
			    for ($i=0 ; $i<5 ; $i++) {
				    $value = reset($value);
				    if (!is_object($value)) {
					    break;
				    }
			    }
		    }

		    $syncData[$field->id] = [
			    'value' => $value
		    ];
	    }

	    $job->fields()->sync($syncData);


	    $job->fetch = true;
	    $job->save();

	    \Flash::success('Done');
	    return \Redirect::refresh();
    }
}
