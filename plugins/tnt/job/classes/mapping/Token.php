<?php namespace TNT\Job\Classes\Mapping;

use GuzzleHttp\Psr7\Response;

class Token implements Mapping {

	public $AccessToken;
	public $AccessTokenExpiresIn;
	public $RefreshToken;
	public $RefreshTokenExpiresIn;
	public $Error;
	public $Message;
	public $array = [];

	function __construct( Response $data ) {
		$xmlResponse = new \SimpleXMLIterator($data->getBody()->__toString());
		for ($xmlResponse->rewind(); $xmlResponse->valid(); $xmlResponse->next()) {

			$this->{$xmlResponse->key()} = $xmlResponse->getChildren()->__toString();
			$this->array[$xmlResponse->key()] = $xmlResponse->getChildren()->__toString();
		}
	}

	/**
	 * @return array
	 */
	public function getArray() {
		return $this->array;
	}
}