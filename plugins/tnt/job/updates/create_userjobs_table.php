<?php namespace Tnt\Job\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class CreateUserjobsTable extends Migration
{
    public function up()
    {
        Schema::create('tnt_job_userjobs', function(Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->integer('user_id');
            $table->integer('job_id');
            $table->string('firstname');
            $table->string('lastname');
            $table->dateTime('dob');
            $table->string('gender');
            $table->string('japaneselevel');
            $table->string('englishlevel');
            $table->string('description');
            $table->timestamps();
            $table->primary(['user_id', 'job_id']);
        });
    }

    public function down()
    {
        Schema::dropIfExists('tnt_job_userjobs');
    }
}
