<?php namespace TNT\Job\Models;

use Model;

/**
 * attribute Model
 */
class Attribute extends Model
{
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * @var string The database table used by the model.
     */
    public $table = 'tnt_job_attributes';

    /**
     * @var array Guarded fields
     */
    protected $guarded = ['*'];

    /**
     * @var array Fillable fields
     */
    protected $fillable = [];

    /**
     * @var array Relations
     */
    public $hasOne = [];
    public $hasMany = [];
    public $belongsTo = [];
    public $belongsToMany = [
        'entities' => ['TNT\Job\Models\Entity', 'table' => 'tnt_job_attribute_strings']
    ];
    public $morphTo = [];
    public $morphOne = [];
    public $morphMany = [];
    public $attachOne = [];
    public $attachMany = [];

    public function getEntity() {

    }
}
