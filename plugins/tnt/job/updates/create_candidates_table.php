<?php namespace TNT\Job\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class CreateCandidatesTable extends Migration
{
    public function up()
    {
        Schema::create('tnt_job_candidates', function(Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->integer('id');
            $table->integer('person_p_owner');
            $table->string('person_p_name');
            $table->string('person_p_registrationdate');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('tnt_job_candidates');
    }
}
