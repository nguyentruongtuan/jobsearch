<?php namespace Tnt\Job\Models;

use Model;

/**
 * option Model
 */
class Option extends Model
{
    /**
     * @var string The database table used by the model.
     */
    public $table = 'tnt_job_options';

    /**
     * @var array Guarded fields
     */
    protected $guarded = ['*'];

    /**
     * @var array Fillable fields
     */
    protected $fillable = [
    	'id',
    	'name',
    	'alias',
    	'parent_id',
    	'type',
    ];

    /**
     * @var array Relations
     */
    public $hasOne = [];
    public $hasMany = [];
    public $belongsTo = [];
    public $belongsToMany = [];
    public $morphTo = [

    ];
    public $morphOne = [];
    public $morphByMany = [
	    'jobs' => [
		    'TNT\Job\Models\Job',
		    'name' => 'entity',
		    'table' => 'tnt_job_option_entity'
	    ],
	    'jobLocation' => [
		    'TNT\Job\Models\Job',
		    'name' => 'locationable',
		    'table' => 'tnt_job_location_entity'
	    ],
	    'jobNationality' => [
		    'TNT\Job\Models\Job',
		    'name' => 'nationable',
		    'table' => 'tnt_job_nationality_entity'
	    ],
    ];
    public $attachOne = [];
    public $attachMany = [];
}
