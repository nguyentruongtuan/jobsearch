<?php namespace Tnt\Job\Console;

use Illuminate\Console\Command;
use League\OAuth2\Client\Token\AccessToken;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;
use TNT\Job\Classes\Provider;
use TNT\Job\Models\Settings;

class Refreshtoken extends Command
{
    /**
     * @var string The console command name.
     */
    protected $name = 'job:refreshtoken';

    /**
     * @var string The console command description.
     */
    protected $description = 'No description provided yet...';

    /**
     * Execute the console command.
     * @return void
     */
    public function fire()
    {
    	trace_log('====> Begin refresh token');
        $provider = new Provider();
		$token = $provider->getRefreshToken();
	    Settings::set('access_token', $token->getToken());
	    Settings::set('refresh_token', $token->getRefreshToken());
	    Settings::set('expires', $token->getExpires());
	    trace_log('====> End refresh token');
    }

    /**
     * Get the console command arguments.
     * @return array
     */
    protected function getArguments()
    {
        return [];
    }

    /**
     * Get the console command options.
     * @return array
     */
    protected function getOptions()
    {
        return [];
    }
}
