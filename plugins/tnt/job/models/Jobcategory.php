<?php namespace TNT\Job\Models;

use Model;

/**
 * jobcategory Model
 */
class Jobcategory extends Model
{
    /**
     * @var string The database table used by the model.
     */
    public $table = 'tnt_job_jobcategories';

    /**
     * @var array Guarded fields
     */
    protected $guarded = ['*'];

    /**
     * @var array Fillable fields
     */
    protected $fillable = [
        'name',
        'title'
    ];

    /**
     * @var array Relations
     */
    public $hasOne = [];
    public $hasMany = [];
    public $belongsTo = [];
    public $belongsToMany = [
        'jobs' => ['TNT\Job\Models\Job', 'table' => 'tnt_job_job_jobcategories']
    ];
    public $morphTo = [];
    public $morphOne = [];
    public $morphMany = [];
    public $attachOne = [];
    public $attachMany = [];

    public function getDetailUrl() {
		return url('category', $this->getKey());
    }
}
