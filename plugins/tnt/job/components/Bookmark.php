<?php namespace Tnt\Job\Components;

use Cms\Classes\ComponentBase;

class Bookmark extends ComponentBase
{
	public $jobId;
	public $job;

    public function componentDetails()
    {
        return [
            'name'        => 'bookmark Component',
            'description' => 'No description provided yet...'
        ];
    }

    public function defineProperties()
    {
	    return [
		    'jobId' => [
			    'title'             => 'Job ID',
			    'description'       => 'Job id slug',
			    'default'           => '{{ :id }}',
			    'type'              => 'text',
		    ]
	    ];
    }

    public function onRun() {
	    $jobId = $this->property('jobId');
	    $this->jobId = $this->page['jobId'] = $jobId;
	    $this->job = \TNT\Job\Models\Job::find($jobId);
	    if ($user = \Auth::getUser()) {
		    $this->page['isBookarked'] =  !!$user->bookmarks()->where('id', $jobId)->first();
	    }

    }

    public function onBookmark() {
    	$id = post('id');
    	$isBookmarked = post('isBookmarked');
    	$user = \Auth::getUser();
    	if ($user) {
    		$user->bookmarks()->detach($id);
    		if (!$isBookmarked) {
			    $user->bookmarks()->attach($id);
		    }

		    \Flash::success('Bookmark success');
	    }

	    return \Redirect::refresh();
    }

}
