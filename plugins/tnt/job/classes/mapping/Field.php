<?php namespace TNT\Job\Classes\Mapping;

class Field extends BaseMapping {

	const ATTRIBUTES = [
		'id' => 'Field.P_Id',
		'name' => 'Field.P_Name',
		'alias' => 'Field.P_Alias',
		'type' => 'Field.P_Type',
		'required' => 'Field.P_Required',
		'max' => 'Field.P_Max',
		'min' => 'Field.P_Min',
		'decimal_fraction' => 'Field.P_DecimalFraction',
		'refer_to' => 'Field.P_ReferTo',
		'resource_type' => 'Field.P_ResourceType',
	];

	public function getAttributes() {
		return self::ATTRIBUTES;
	}

}