<?php namespace TNT\Job\Classes\Mapping;

use GuzzleHttp\Psr7\Response;

class BaseMapping implements Mapping {

	public $provider;
	public $array;

	public function __construct( Response $data ) {
		$xmlResponse = simplexml_load_string($data->getBody()->__toString());
		$parsed = \GuzzleHttp\json_decode(\GuzzleHttp\json_encode($xmlResponse));

		if (in_array($parsed->Code, ['100', '103', '401'])) {
			$this->array["Error"] = $parsed->Code;
			$this->array["Message"] = $data->getStatusCode();
			return;
		}

		if (is_array($parsed->Item)) {

			foreach ($parsed->Item as $item) {
				$record = [];
				foreach ($this->getAttributes() as $attribute => $apiAttribute)
				{
					if (is_object($item->$apiAttribute)) {
						$value = reset($item->$apiAttribute);
						if ($value) {
							$value = reset($value);
						}


						$record[$attribute] = ($value );
					} else {
						$record[$attribute] = $item->$apiAttribute;
					}

				}
				$this->array[] = $record;
			}

		} else {
			$record = [];
			foreach ($this->getAttributes() as $attribute => $apiAttribute)
			{
				$record[$attribute] = $parsed->Item->$apiAttribute;
			}
			$this->array[] = $record;
		}
	}

	public function getAttributes() {
		return [];
	}

	/**
	 * @return array
	 */
	public function getArray() {
		return $this->array;
	}

	public function getFirst() {
		if (count($this->array)) {
			return $this->array[0];
		}

		return null;
	}
}