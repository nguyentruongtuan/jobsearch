<?php namespace Tnt\Job\Components;

use Cms\Classes\ComponentBase;

class Jobdetail extends ComponentBase
{
	public $job;
	public $fields = [];
    public function componentDetails()
    {
        return [
            'name'        => 'Job detail',
            'description' => 'No description provided yet...'
        ];
    }

    public function defineProperties()
    {
        return [
        	'jobId' => [
		        'title'             => 'Job ID',
		        'description'       => 'Job id slug',
		        'default'           => '{{ :id }}',
		        'type'              => 'text',
	        ]
        ];
    }

    public function onRun() {
		$jobId = $this->property('jobId');
		$job = \TNT\Job\Models\Job::find($jobId);
		if (!$job) {
			return \Redirect::to('');

		}
		$this->job = $this->page['model'] = $job;

		foreach ($job->fields_pivot_model as $field) {
			$this->fields[$field->code] = $field->pivot->value;
		}

		$this->page['fields'] = $this->fields;
    }

    public function getFieldValue($code) {
	    return $this->job->fields_pivot_model->where('code', $code)->first()->pivot->value;
    }
}
