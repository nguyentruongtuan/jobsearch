<?php namespace TNT\Job\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class CreateFieldValuesTable extends Migration
{
    public function up()
    {
        Schema::create('tnt_job_field_values', function(Blueprint $table) {
            $table->engine = 'InnoDB';
//            $table->increments('id');
            $table->integer('field_id')->unsigned();
            $table->integer('resource_id')->unsigned();
            $table->longText('value');
            $table->primary(['field_id', 'resource_id']);
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('tnt_job_field_values');
    }
}
