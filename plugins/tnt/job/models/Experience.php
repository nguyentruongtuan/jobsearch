<?php namespace Tnt\Job\Models;

use Model;

/**
 * exprience Model
 */
class Experience extends Model
{
    /**
     * @var string The database table used by the model.
     */
    public $table = 'tnt_job_expriences';

    /**
     * @var array Guarded fields
     */
    protected $guarded = ['*'];

    /**
     * @var array Fillable fields
     */
    protected $fillable = [
    	'title',
    	'type_of_work',
    	'location',
    	'from',
    	'to',
    	'description',
    	'type',
    ];

    protected $jsonable = [
	    'title',
	    'type_of_work',
	    'location',
	    'from',
	    'to',
	    'description',
	    'type',
    ];

    /**
     * @var array Relations
     */
    public $hasOne = [
    ];
    public $hasMany = [];
    public $belongsTo = [
	    'user' => 'Rainlab\User\Models\User'
    ];
    public $belongsToMany = [];
    public $morphTo = [];
    public $morphOne = [];
    public $morphMany = [];
    public $attachOne = [];
    public $attachMany = [];
}
