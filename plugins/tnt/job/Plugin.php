<?php namespace TNT\Job;

use Backend;
use Clake\UserExtended\Classes\GroupManager;
use Clake\Userextended\Models\UserExtended;
use Illuminate\Support\Facades\Artisan;
use System\Classes\PluginBase;
use System\Classes\SettingsManager;
use Rainlab\User\Models\User;
use Tnt\Job\Console\Addcategory;
use Tnt\Job\Console\Addlocation;
use Tnt\Job\Console\Addoption;
use TNT\Job\Console\Fetch;
use Tnt\Job\Console\Fetchrecruiter;
use Tnt\Job\Console\Refreshtoken;
use TNT\Job\Console\FieldDefault;
use Event;
use Log;
use TNT\Job\Models\Entity;
use TNT\Job\Models\Profile;

/**
 * Job Plugin Information File
 */
class Plugin extends PluginBase
{
    public $require = [
        'Rainlab.User',
        'Clake.UserExtended'
    ];
    /**
     * Returns information about this plugin.
     *
     * @return array
     */
    public function pluginDetails()
    {
        return [
            'name'        => 'Job',
            'description' => 'A plugin for sync job from another system',
            'author'      => 'TNT',
            'icon'        => 'icon-leaf'
        ];
    }

    /**
     * Register method, called when the plugin is first registered.
     *
     * @return void
     */
    public function register()
    {
        $this->registerConsoleCommand('tnt.job:default-job-feild', FieldDefault::class);
        $this->registerConsoleCommand('tnt.job:fetch', Fetch::class);
        $this->registerConsoleCommand('tnt.job:fetchrecruiter', Fetchrecruiter::class);
        $this->registerConsoleCommand('tnt.job:refreshtoken', Refreshtoken::class);
        $this->registerConsoleCommand('tnt.job:addcategory', Addcategory::class);
        $this->registerConsoleCommand('tnt.job:addlocation', Addlocation::class);
        $this->registerConsoleCommand('tnt.job:addoption', Addoption::class);
    }

    /**
     * Boot method, called right before the request route.
     *
     * @return array
     */
    public function boot()
    {

        User::extend(function ($model) {
            $model->hasOne['candidate'] = ['TNT\Job\Models\Candidate'];
            $model->hasOne['recruiter'] = ['TNT\Job\Models\Recruiter'];
            // $model->hasOne['entity'] = ['TNT\Job\Models\Entity', 'key' => 'source_id'];
            $model->hasOne['profile'] = ['TNT\Job\Models\Profile'];
            $model->attachMany['resume'] = 'System\Models\File';

            $model->belongsToMany['bookmarks'] = [
	            'TNT\Job\Models\Job',
	            'table' => 'tnt_job_bookmarks',
	            'otherKey' => 'resource_id'
            ];

            $model->belongsToMany['job_applied'] = [
	            'TNT\Job\Models\Job',
	            'table' => 'tnt_job_userjobs'
            ];

	        $pivotFields = ['lastname', 'firstname', 'gender', 'dob', 'japaneselevel', 'englishlevel', 'description'];
            foreach ($pivotFields as $pivotField) {
	            $model->belongsToMany["pivot_" . $pivotField] = [
		            'TNT\Job\Models\Job',
		            'table' => 'tnt_job_userjobs',
		            'pivot' => [$pivotField]
	            ];
            }


	        $model->hasMany['experiences'] = [
		        'TNT\Job\Models\Experience'
	        ];

            $model->bindEvent('model.beforeDelete', function() use ($model) {
                $model->profile && $model->profile->delete();
                $model->candidate && $model->candidate->delete();
                $model->recruiter && $model->recruiter->delete();
            });
        });

        UserExtended::created(function($user) {

            $accountType = post('account_type');
            if ($accountType == 'candidate') {
                $group = GroupManager::findGroup('candidate');
                $group->users()->add($user);
            }
        });
    }

    /**
     * Registers any front-end components implemented in this plugin.
     *
     * @return array
     */
    public function registerComponents()
    {
        return [
            'TNT\Job\Components\Profile' => 'profile',
            'TNT\Job\Components\Job' => 'job',
            'TNT\Job\Components\Relatedjob' => 'relatedjob',
            'TNT\Job\Components\Jobdetail' => 'jobdetail',
            'TNT\Job\Components\Bookmark' => 'bookmark',
            'TNT\Job\Components\Bookmarklist' => 'bookmarklist',
            'TNT\Job\Components\Applyjob' => 'applyjob',
            'TNT\Job\Components\Applyjoblist' => 'applyjoblist',
            'TNT\Job\Components\Category' => 'jobcategory',
            'TNT\Job\Components\Search' => 'jobsearch',
            'TNT\Job\Components\Experience' => 'experience',
            'TNT\Job\Components\Uploadcv' => 'uploadcv'
        ];
    }

    /**
     * Registers any back-end permissions used by this plugin.
     *
     * @return array
     */
    public function registerPermissions()
    {

        return [
            'tnt.job.access' => [ 'tab' => 'tnt.job:plugin::lang::settings:tab', 'label' => 'Manage jobs' ],
	        'tnt.job.oauth' => [ 'tab' => 'tnt.job:plugin::lang::settings:tab', 'label' => 'Manage API'],
	        'tnt.job.category' => [ 'tab' => 'tnt.job:plugin::lang::settings:tab', 'label' => 'Manage Category'],
	        'tnt.job.job_fields' => [ 'tab' => 'tnt.job:plugin::lang::settings:tab', 'label' => 'Manage Job Fields'],
        ];
    }

    /**
     * Registers back-end navigation items for this plugin.
     *
     * @return array
     */
    public function registerNavigation()
    {
        return [
            'job' => [
                'label'       => 'Job',
                'url'         => Backend::url('tnt/job/job'),
                'icon'        => 'icon-book',
                'permissions' => ['tnt.job.*'],
                'order'       => 500,
	            'sideMenu' => [
	            	'partition' => [
	            		'label' => 'Connect',
	            		'icon' => 'icon-rotate-right',
	            		'url' => Backend::url('tnt/job/partition'),
	            		'permissions' => ['tnt.job.oauth'],
		            ],
		            'job' => [
			            'label' => 'Job',
			            'icon' => 'icon-database',
			            'url' => Backend::url('tnt/job/job'),
			            'permissions' => ['tnt.job.*'],
		            ],
		            'category' => [
			            'label' => 'Category',
			            'icon' => 'icon-object-group',
			            'url' => Backend::url('tnt/job/jobcategory'),
			            'permissions' => ['tnt.job.category'],
		            ],
		            'candidate' => [
			            'label' => 'Candidate',
			            'icon' => 'icon-newspaper-o',
			            'url' => Backend::url('tnt/job/candidate'),
			            'permissions' => ['tnt.job.*'],
		            ],
		            'recruiter' => [
			            'label' => 'Recruiter',
			            'icon' => 'icon-user-plus',
			            'url' => Backend::url('tnt/job/recruiter'),
			            'permissions' => ['tnt.job.*'],
		            ],
		            'field' => [
			            'label' => 'Field',
			            'icon' => 'icon-edit',
			            'url' => Backend::url('tnt/job/field'),
			            'permissions' => ['tnt.job.job_fields'],
		            ],
	            ]
            ],
        ];
    }

    public function registerSettings()
    {
        return [
            'settings' => [
                'label'       => 'tnt.job::lang.settings.menu_label',
                'description' => 'tnt.job::lang.settings.menu_description',
                'category'    => SettingsManager::CATEGORY_SYSTEM,
                'icon'        => 'icon-cog',
                'class'       => 'TNT\Job\Models\Settings',
                'order'       => 500,
                'permissions' => ['tnt.job.oauth']
            ]
        ];
    }

	public function registerSchedule($schedule)
	{
		$schedule->command('job:refreshtoken')->everyFiveMinutes()->when(function(){
			return true;
		});

		$schedule->command('job:fetch')->everyFiveMinutes()->when(function(){
			return true;
		});
	}
}
