<?php namespace Tnt\Job\Components;

use Cms\Classes\ComponentBase;
use Input;
use League\Flysystem\Exception;

class Uploadcv extends ComponentBase
{
    public function componentDetails()
    {
        return [
            'name'        => 'uploadcv Component',
            'description' => 'No description provided yet...'
        ];
    }

    public function defineProperties()
    {
        return [];
    }

    public function onUploadCV() {
	    $user = \Auth::getUser();
	    if ($user) {
	    	try {
			    $user->resume()->create(['data' => Input::file('cv')]);
			    return \Redirect::refresh();
		    } catch (\Exception $e) {
	    	    \Flash::error('Invalid file type');
				return \Redirect::refresh();
		    }
	    }
    }
}
