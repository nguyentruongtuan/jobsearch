$(function () {
    $('.tab-link').click(function () {
        $('.tab-link').removeClass('active');
        var el = $(this);
        el.addClass('active');

        var tabContent = el.attr('data-tab');
        $('.tab-content').removeClass('active');
        $('#'+tabContent).addClass('active')
    });

});