<?php namespace Tnt\Job\Components;

use Cms\Classes\ComponentBase;

class Experience extends ComponentBase
{
	public $list;

    public function componentDetails()
    {
        return [
            'name'        => 'experience Component',
            'description' => 'No description provided yet...'
        ];
    }

    public function defineProperties()
    {
        return [
	        'jobId' => [
		        'title'             => 'Job ID',
		        'description'       => 'Job id slug',
		        'default'           => '{{ :id }}',
		        'type'              => 'text',
	        ]
        ];
    }

    public function onRun() {
	    $this->addJs('/plugins/tnt/job/assets/js/exp.js');

    }

    public function onSave() {
    	$data = post();

	    $user = \Auth::getUser();
	    if ($user) {
			$user->experiences()->create($data);

			return \Redirect::refresh();
	    }
    }

    public function onEdit() {
    	$id = post('expId');
	    $exp = \Tnt\Job\Models\Experience::find($id);
	    if ($exp) {
	    	return $this->renderPartial('experience::edit', ['exp' => $exp]);
	    }
    }

    public function onUpdateExp() {
    	$data = post();
	    $exp = \Tnt\Job\Models\Experience::find($data['id']);
	    if ($exp) {
	    	$exp->fill($data);
	    	$exp->save();
	    	return \Redirect::refresh();
	    }

	    return 'failed';
    }

    public function onDeleteExp() {
	    $data = post();
	    $exp = \Tnt\Job\Models\Experience::find($data['id']);
	    if ($exp) {
		    $exp->delete();
		    return \Redirect::refresh();
	    }
    }
}
