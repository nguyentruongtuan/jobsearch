<?php namespace TNT\Job\Models;

use Model;

/**
 * field Model
 */
class Field extends Model
{
    /**
     * @var string The database table used by the model.
     */
    public $table = 'tnt_job_fields';

    /**
     * @var array Guarded fields
     */
    protected $guarded = ['*'];

    /**
     * @var array Fillable fields
     */
    protected $fillable = [
    	'id',
    	'name',
    	'code',
    	'alias',
    	'type',
    	'required',
    	'max',
    	'min',
    	'decimal_fraction',
    	'refer_to',
    	'resource_type',
    ];

    /**
     * @var array Relations
     */
    public $hasOne = [];
    public $hasMany = [];
    public $belongsTo = [];
    public $belongsToMany = [
    	'resources' => [
    		'TNT\Job\Models\Job',
		    'table' => 'tnt_job_field_values',
		    'otherKey' => 'resource_id',
	    ]
    ];
    public $morphTo = [];
    public $morphOne = [];
    public $morphMany = [];
    public $attachOne = [];
    public $attachMany = [];


}
