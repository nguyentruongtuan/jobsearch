<?php namespace TNT\Job\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class CreateResumesTable extends Migration
{
    public function up()
    {
        Schema::create('tnt_job_resumes', function(Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->integer('candidate_id');
            $table->dateTime('registration_date');
            $table->dateTime('update_date');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('tnt_job_resumes');
    }
}
