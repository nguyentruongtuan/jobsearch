<?php namespace Tnt\Job\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class CreateLocationsTable extends Migration
{
    public function up()
    {
        Schema::create('tnt_job_locations', function(Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->integer('id');
            $table->string('name');
	        $table->unsignedInteger('entity_id')->nullable();
	        $table->string('entity_type')->nullable();
	        $table->primary(['id', 'entity_id', 'entity_type']);
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('tnt_job_locations');
    }
}
