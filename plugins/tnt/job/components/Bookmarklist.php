<?php namespace Tnt\Job\Components;

use Cms\Classes\ComponentBase;

class Bookmarklist extends ComponentBase
{
	public $list;
    public function componentDetails()
    {
        return [
            'name'        => 'bookmarklist Component',
            'description' => 'No description provided yet...'
        ];
    }

    public function defineProperties()
    {
        return [];
    }

	public function onRun() {
		$user = \Auth::getUser();
		if ($user) {
			$this->list = $this->page['list'] = $user->bookmarks()->get();
		}
	}

	public function onBookmark() {
		$id = post('id');
		$isBookmarked = post('isBookmarked');
		$user = \Auth::getUser();
		if ($user) {
			$user->bookmarks()->detach($id);
			if (!$isBookmarked) {
				$user->bookmarks()->attach($id);
			}

			\Flash::success('Bookmark success');
		}

		return \Redirect::refresh();
	}

	function onSearchByCategory() {
		$jobId = post('id');

		$categoryRelation = \Db::table('tnt_job_option_entity')->where('entity_id', $jobId)->first();

		$url = $this->controller->pageUrl('advance-search') . '?' . http_build_query(['category' => $categoryRelation->option_id]);
		return \Redirect::to($url);
	}
}
