<?php namespace TNT\Job\Models;

use Model;

/**
 * field_value Model
 */
class FieldValue extends Model
{
    /**
     * @var string The database table used by the model.
     */
    public $table = 'tnt_job_field_values';

    /**
     * @var array Guarded fields
     */
    protected $guarded = ['*'];

    /**
     * @var array Fillable fields
     */
    protected $fillable = [
    	'field_id',
    	'resource_id',
    	'value',
    ];

    /**
     * @var array Relations
     */
    public $hasOne = [];
    public $hasMany = [];
    public $belongsTo = [];
    public $belongsToMany = [];
    public $morphTo = [];
    public $morphOne = [];
    public $morphMany = [];
    public $attachOne = [];
    public $attachMany = [];
}
