<?php namespace Tnt\Job\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class CreateClientsTable extends Migration
{
    public function up()
    {
        Schema::create('tnt_job_clients', function(Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->integer('id');
            $table->integer('client_p_owner');
            $table->string('client_p_registrationdate');
            $table->string('client_p_name');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('tnt_job_clients');
    }
}
