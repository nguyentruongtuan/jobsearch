(function() {
    $(document).ready(function(){
        $("#see-more-job").click(function(){
            $("#filter-job .more").slideToggle("fast");
        });
        $("#see-more-location").click(function(){
            $("#filter-location .more").slideToggle("fast");
        });
        $("#see-more-language").click(function(){
            $("#filter-language .more").slideToggle("fast");
        });
    });
}());