<?php namespace Tnt\Job\Models;

use Model;

/**
 * location Model
 */
class Location extends Model
{
    /**
     * @var string The database table used by the model.
     */
    public $table = 'tnt_job_locations';

    /**
     * @var array Guarded fields
     */
    protected $guarded = ['*'];

    /**
     * @var array Fillable fields
     */
    protected $fillable = [
    	'id',
    	'name'
    ];

    /**
     * @var array Relations
     */
    public $hasOne = [];
    public $hasMany = [];
    public $belongsTo = [];
    public $belongsToMany = [];
    public $morphTo = [
    	'entity' => []
    ];
    public $morphOne = [];
    public $morphMany = [];
    public $attachOne = [];
    public $attachMany = [];
}
