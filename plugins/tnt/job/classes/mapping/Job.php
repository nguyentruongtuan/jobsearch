<?php namespace TNT\Job\Classes\Mapping;

class Job extends BaseMapping {

	const ATTRIBUTES = [
		'id' => 'Job.P_Id',
		'name' => 'Job.P_Position',
		'owner_id' => 'Job.P_Owner',
		'recruiter_id' => 'Job.P_Recruiter',
		'registration_date' => 'Job.P_RegistrationDate',
		'registered_by' => 'Job.P_RegisteredBy',
		'updated_date' => 'Job.P_UpdateDate',
		'updated_by' => 'Job.P_UpdatedBy',
		'category' => 'Job.P_JobCategory',
		'memo' => 'Job.P_Memo',
		'publish' => 'Job.P_Publish',
		'industry' => 'Job.P_Industry',
		'min_salary' => 'Job.P_MinSalary',
		'max_salary' => 'Job.P_MaxSalary',
	];

	public function getAttributes() {
		return self::ATTRIBUTES;
	}

}