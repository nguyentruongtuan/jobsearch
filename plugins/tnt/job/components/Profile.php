<?php namespace TNT\Job\Components;

use Cms\Classes\ComponentBase;
use Auth;
use Flash;
use Redirect;

class Profile extends ComponentBase
{
    public function componentDetails()
    {
        return [
            'name'        => 'profile Component',
            'description' => 'No description provided yet...'
        ];
    }

    public function onRun()
    {
        $user = Auth::getUser();
        if ($user) {
            $profile = $user->profile()->getResults();

            $this->page['profile'] = $profile;
        }
    }

    public function defineProperties()
    {
        return [];
    }

    public function onSave()
    {
        if (!$user = Auth::getUser()) {
            return;
        }

        if (!$profile = $user->profile()->getResults()) {
            $user->profile()->create(post());

        } else {
            $profile->fill(post());
            $profile->save();
        }

        Flash::success('Save success');

        return Redirect::to('profile');

    }
}
