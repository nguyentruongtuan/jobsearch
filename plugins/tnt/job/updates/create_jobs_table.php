<?php namespace TNT\Job\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class CreateJobsTable extends Migration
{
    public function up()
    {
        Schema::create('tnt_job_jobs', function(Blueprint $table) {
            $table->engine = 'InnoDB';
            //$table->increments('id');
            $table->integer('id');
            $table->integer('owner_id');
            $table->string('name')->nullable();
            $table->integer('recruiter_id');
            $table->dateTime('registration_date')->nullable();
            $table->integer('registered_by')->nullable();
            $table->decimal('min_salary', 10, 2)->default(0);
            $table->decimal('max_salary', 10, 2)->default(0);
            $table->dateTime('update_date')->nullable();
            $table->integer('updated_by')->nullable();
            $table->boolean('fetch')->default(false);
            $table->timestamps();
            $table->primary('id');
        });
    }

    public function down()
    {
        Schema::dropIfExists('tnt_job_jobs');
    }
}
