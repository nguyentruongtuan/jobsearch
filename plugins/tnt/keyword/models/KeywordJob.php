<?php namespace TNT\Keyword\Models;

use Model;

/**
 * keyword_job Model
 */
class KeywordJob extends Model
{
    /**
     * @var string The database table used by the model.
     */
    public $table = 'tnt_keyword_keyword_jobs';

    /**
     * @var array Guarded fields
     */
    protected $guarded = ['*'];

    /**
     * @var array Fillable fields
     */
    protected $fillable = [];

    /**
     * @var array Relations
     */
    public $hasOne = [];
    public $hasMany = [];
    public $belongsTo = [];
    public $belongsToMany = [];
    public $morphTo = [];
    public $morphOne = [];
    public $morphMany = [];
    public $attachOne = [];
    public $attachMany = [];
}
