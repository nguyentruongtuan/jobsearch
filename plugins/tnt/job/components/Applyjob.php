<?php namespace Tnt\Job\Components;

use Cms\Classes\ComponentBase;
use Faker\Provider\DateTime;
use Input;
use League\Flysystem\Exception;
use October\Rain\Exception\ValidationException;
use Clake\UserExtended\Classes\UserSettingsManager;

class Applyjob extends ComponentBase
{
	public $info = [];

	public $job;

    public function componentDetails()
    {
        return [
            'name'        => 'applyjob Component',
            'description' => 'No description provided yet...'
        ];
    }

    public function defineProperties()
    {
        return [
	        'jobId' => [
		        'title'             => 'Job ID',
		        'description'       => 'Job id slug',
		        'default'           => '{{ :id }}',
		        'type'              => 'text',
	        ]
        ];
    }

    public function onRun() {
	    $this->addJs('/plugins/tnt/job/assets/js/applyjob.js');

	    $job = \TNT\Job\Models\Job::find($this->property('jobId'));
	    $this->job = $this->page['job'] = $job;

	    $user = \Auth::getUser();
	    if ($user) {
	    	$this->page['user'] = $user;
	    }
    }

	public function onApply() {
    	$id = $this->property('jobId');

    	// validate completed info and throw exception
    	$user = \Auth::getUser();
    	if ($user) {

		    $setting = (array)\GuzzleHttp\json_decode($user->settings);
			foreach (['lastname', 'description', 'gender', 'japaneselevel', 'englishlevel', 'dobday', 'dobmonth', 'dobyear'] as $key) {
				if (!$setting || !key_exists($key, $setting)  || !$setting[$key]) {
					throw new \ValidationException(['name' => 'Your profile is incomplete, please fill it']);
				}
			}
//
//			if ($user->resume()->count() == 0) {
//				throw new \ValidationException(['name' => 'You have no CV, please upload one']);
//			}

		    $file = new \System\Models\File;
		    $file->data = Input::file('resume');
		    if (\Input::file('resume')) {
		    	$file->save();
			    $user->resume()->add($file);
			}
    		$this->info = \Session::get('apply-info');
		    if (!$this->info) {

			    $setting = \GuzzleHttp\json_decode($user->settings);
			    $this->info = [
				    'firstname' => $user->surname,
				    'lastname' => $setting->lastname,
				    'description' => $setting->description,
				    'gender' => $setting->gender,
				    'japaneselevel' => $setting->japaneselevel,
				    'englishlevel' => $setting->englishlevel,
				    'dob' => date_create("{$setting->dobday}-{$setting->dobmonth}-{$setting->dobyear}"),
			    ];
		    }
    		$user->job_applied()->detach($id);
		    $user->job_applied()->attach($id, $this->info);

    		return \Redirect::to('apply-success');
	    }
    }

    public function onSaveInfo() {

	    $validator = \Validator::make(
	    	post(),
		    [
		    	'firstname' => 'required',
		    	'lastname' => 'required',
		    	'dobday' => ['required', 'numeric', 'min:1', 'max:31'],
			    'dobmonth' => ['required', 'numeric', 'min:1', 'max:12'],
			    'dobyear' => ['required', 'numeric', 'min:1900', 'max:2017']
		    ]
	    );

	    if ($validator->fails()) {
		    $messages = $validator->messages()->getMessages();
		    foreach ($messages as $key => $value) {
			    throw new \ValidationException([$key => $value[0]]);
		    }

	    }

	    $user = \Auth::getUser();
	    if ($user) {

	    	$user->surname = post('firstname');
	    	$user->save();

		    $settingsManager = UserSettingsManager::currentUser();
		    foreach(post() as $key=>$value)
		    {
			    if($key=="_session_key" || $key=="_token" || $key=="name" || $key=="username" || $key=="email" || $key=="password" || $key=="password_confirmation")
				    continue;

			    if($settingsManager->isSetting($key))
			    {
				    /** @var $validator bool|Validator\ */
				    $validator = $settingsManager->setSetting($key, $value);
				    if($validator !== true)
				    {
					    /*
						 * This means validation failed and the setting was NOT set.
						 * $validator is a Validator instance
						 */
					    return $validator;
				    }
			    }
		    }

		    $settingsManager->save();



		    \Flash::success('rainlab.user::lang.account.success_saved');
	    } else {
		    \Flash::error('Have not logged in');
	    }

    }

    public function onUploadCV() {
	    $user = \Auth::getUser();
	    if ($user) {
		    try {
			    $user->resume()->create(['data' => \Input::file('cv')]);
			    \Flash::success('Upload CV successfully');
		    } catch (\Exception $e) {

			    throw new \ValidationException(['file' => $e->getMessage()]);
		    }
	    }
    }

	public function onSaveExperience() {
		$data = post();

		$user = \Auth::getUser();
		if ($user) {
			$result = $user->experiences()->create($data);
		}
	}

}
