<?php namespace Tnt\Job\Components;

use Cms\Classes\ComponentBase;
use TNT\Job\Models\Jobcategory;

class Category extends ComponentBase
{
	public $category;
    public function componentDetails()
    {
        return [
            'name'        => 'category Component',
            'description' => 'No description provided yet...'
        ];
    }

    public function defineProperties()
    {
	    $jobCategories = Jobcategory::all();
	    $arr = [];
	    foreach ($jobCategories as $job_category) {
		    $arr[$job_category->id] = $job_category->name;
	    }

        return [
	        'id' => [
		        'title'             => 'Category ID',
		        'description'       => 'Job Category ID',
		        'default'           => '',
		        'type'              => 'dropdown',
		        'options'           => $arr
	        ]
        ];
    }

    public function onRun() {
	    $id = $this->property('id');
	    $this->category = Jobcategory::find($id);

	    $this->page['category'] = $this->category;
    }
}
