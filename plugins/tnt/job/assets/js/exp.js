$(function () {
    $('.btn-exp').click(function () {
        var $form = $('.add-new-exp');
        $form.toggleClass('hide');

        $form.find('.btn-cancel').one('click', function () {
            $form.toggleClass('hide');
        }.bind(this));
    });

    $('.btn-edu').click(function () {
        var $form = $('.add-new-edu');
        $form.toggleClass('hide');

        $form.find('.btn-cancel').one('click', function () {
            $form.toggleClass('hide');
        }.bind(this));
    });

    $('.btn-exp-edit').click(function () {
        var $editForm = $("#" + $(this).attr('edit-form'));
        $editForm.find('.btn-cancel').one('click', function () {
            $editForm.toggleClass('hide');
        });

        if ($editForm) {
            $editForm.toggleClass('hide');


        }
    });
});