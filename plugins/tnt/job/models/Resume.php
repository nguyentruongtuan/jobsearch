<?php namespace TNT\Job\Models;

use Model;

/**
 * resume Model
 */
class Resume extends Model
{
    /**
     * @var string The database table used by the model.
     */
    public $table = 'tnt_job_resumes';

    /**
     * @var array Guarded fields
     */
    protected $guarded = ['*'];

    /**
     * @var array Fillable fields
     */
    protected $fillable = [];

    /**
     * @var array Relations
     */
    public $hasOne = [
        'entity' => ['TNT\Job\Models\Entity']
    ];
    public $hasMany = [];
    public $belongsTo = [
        'candidate' => 'TNT\Job\Models\Candidate'
    ];
    public $belongsToMany = [];
    public $morphTo = [];
    public $morphOne = [];
    public $morphMany = [];
    public $attachOne = [];
    public $attachMany = [];
}
