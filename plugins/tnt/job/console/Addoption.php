<?php namespace Tnt\Job\Console;

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;

class Addoption extends Command
{
    /**
     * @var string The console command name.
     */
    protected $name = 'job:addoption';

    /**
     * @var string The console command description.
     */
    protected $description = 'No description provided yet...';

    protected $currentType;

    /**
     * Execute the console command.
     * @return void
     */
    public function fire()
    {
    	// @TODO: get option xml from API
	    $options = simplexml_load_file(base_path() . '/option.xml');

	    $options = json_decode(json_encode($options));
	    $items = $options->{'Item'};
	    $categories = null;

	    foreach ($items as $item) {
	    	$this->currentType = $item->{'Option.P_Id'};
		    $categories = $item->Items;

		    $cats[] = [
			    'id' => $item->{'Option.P_Id'},
			    'name' => $item->{'Option.P_Name'},
			    'alias' => $item->{'Option.P_Alias'},
			    'parent_id' => $item->{'Option.P_ParentId'},
			    'type' => $this->currentType,
		    ];

		    $cats = [];
		    if (key_exists('Item', $categories)) {
			    $this->addItem($categories->Item, $cats);
		    }


		    \Db::table('tnt_job_options')->insert($cats);
	    }


	    $debugger = null;
    }

	function addItem($list, &$cats) {
		foreach ($list as $key => $value) {
			if (!is_object($value)) {
				continue;
			}

			if (count((array)$value) == 0) {
				continue;
			}
			$cats[] = [
				'id' => $value->{'Option.P_Id'},
				'name' => $value->{'Option.P_Name'},
				'alias' => $value->{'Option.P_Alias'},
				'parent_id' => $value->{'Option.P_ParentId'},
				'type' => $this->currentType,
			];


			if (key_exists('Item', $value->Items)) {
				$this->addItem($value->Items->Item, $cats);
			}

		}
	}

    /**
     * Get the console command arguments.
     * @return array
     */
    protected function getArguments()
    {
        return [];
    }

    /**
     * Get the console command options.
     * @return array
     */
    protected function getOptions()
    {
        return [];
    }
}
