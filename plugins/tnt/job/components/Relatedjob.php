<?php namespace Tnt\Job\Components;

use Cms\Classes\ComponentBase;

class Relatedjob extends ComponentBase
{
	public $job;

    public function componentDetails()
    {
        return [
            'name'        => 'relatedjob Component',
            'description' => 'No description provided yet...'
        ];
    }

    public function defineProperties()
    {
	    return [
		    'jobId' => [
			    'title'             => 'Job ID',
			    'description'       => 'Job id slug',
			    'default'           => '{{ :id }}',
			    'type'              => 'text',
		    ]
	    ];
    }

    public function onRun() {
		$id = $this->property('jobId');
		$job = \TNT\Job\Models\Job::find($id);
		$this->job = $this->page['job'] = $job;
    }
}
