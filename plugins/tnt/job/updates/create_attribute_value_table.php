<?php namespace TNT\Job\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class CreateAttributeStringsTable extends Migration
{
    public function up()
    {
        Schema::create('tnt_job_attribute_value', function(Blueprint $table) {
            $table->engine = 'InnoDB';
            // $table->increments('id');
            $table->integer('attribute_id')->unsigned();
            $table->integer('entity_id')->unsigned();
            $table->string('string_value')->nullable();
            $table->integer('integer_value')->nullable();
            $table->dateTime('datetime_value')->nullable();
            $table->string('localization_code');
            $table->timestamps();
            $table->primary(['attribute_id', 'entity_id']);
        });
    }

    public function down()
    {
        Schema::dropIfExists('tnt_job_attribute_value');
    }
}
