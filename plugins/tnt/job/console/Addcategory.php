<?php namespace Tnt\Job\Console;

use Illuminate\Console\Command;
use League\Flysystem\Exception;
use Psy\Exception\ErrorException;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;
use TNT\Job\Models\Jobcategory;

class Addcategory extends Command
{
    /**
     * @var string The console command name.
     */
    protected $name = 'job:addcategory';

    /**
     * @var string The console command description.
     */
    protected $description = 'No description provided yet...';

    /**
     * Execute the console command.
     * @return void
     */
    public function fire()
    {
        $this->output->writeln('Hello world!');

        $options = simplexml_load_file(base_path() . '/option.xml');

        $options = json_decode(json_encode($options));
        $items = $options->{'Item'};
        $categories = null;
         for ($i = 0 ; $i < count($items) ; $i++) {
             $item = $items[$i];
             if ($item->{'Option.P_Id'} == 29) {
                 $categories = $item->Items;
                 break;
             }
         }

        $cats = [];
        $this->addItem($categories->Item, $cats);

        \Db::table('tnt_job_jobcategories')->insert($cats);

        $debugger = null;
    }

    function addItem($list, &$cats) {
        foreach ($list as $key => $value) {
            if (!is_object($value)) {
                continue;
            }

            if (count((array)$value) == 0) {
                continue;
            }
            $cats[] = [
                'name' => $value->{'Option.P_Name'},
                'title' => $value->{'Option.P_Alias'},
            ];


            if (isset($value->Items->Item)) {
                $this->addItem($value->Items->Item, $cats);
            }

        }
    }


    /**
     * Get the console command arguments.
     * @return array
     */
    protected function getArguments()
    {
        return [];
    }

    /**
     * Get the console command options.
     * @return array
     */
    protected function getOptions()
    {
        return [];
    }
}
