<?php namespace Tnt\Job\Controllers;

use BackendMenu;
use Backend\Classes\Controller;

/**
 * Candidate Back-end Controller
 */
class Candidate extends Controller
{
    public $implement = [
        'Backend.Behaviors.FormController',
        'Backend.Behaviors.ListController'
    ];

    public $formConfig = 'config_form.yaml';
    public $listConfig = 'config_list.yaml';

    public function __construct()
    {
        parent::__construct();

        BackendMenu::setContext('TNT.Job', 'job', 'candidate');
    }
}
