<?php namespace Tnt\Job\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class CreateOptionsTable extends Migration
{
    public function up()
    {
        Schema::create('tnt_job_options', function(Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->integer('id');
            $table->string('name');
            $table->string('alias');
            $table->integer('parent_id');
            $table->integer('type');
            $table->timestamps();
        });

	    Schema::create('tnt_job_option_entity', function(Blueprint $table) {
		    $table->engine = 'InnoDB';
		    $table->unsignedInteger('option_id')->index('option_id_idx');
		    $table->unsignedInteger('entity_id')->index('entity_id_idx');
		    $table->string('entity_type')->index('entity_type_idx');
		    $table->primary(['option_id', 'entity_id', 'entity_type'], 'option_entity_pk');
	    });

	    Schema::create('tnt_job_location_entity', function(Blueprint $table) {
		    $table->engine = 'InnoDB';
		    $table->unsignedInteger('option_id')->index('option_id_idx');
		    $table->unsignedInteger('locationable_id')->index('locationable_id_idx');
		    $table->string('locationable_type')->index('locationable_type_idx');
		    $table->primary(['option_id', 'locationable_id', 'locationable_type'], 'location_entity_pk');
	    });

	    Schema::create('tnt_job_nationality_entity', function(Blueprint $table) {
		    $table->engine = 'InnoDB';
		    $table->unsignedInteger('option_id')->index('option_id_idx');
		    $table->unsignedInteger('nationable_id')->index('nationable_id_idx');
		    $table->string('nationable_type')->index('nationable_type_idx');
		    $table->primary(['option_id', 'nationable_id', 'nationable_type'], 'nationable_entity_pk');
	    });

    }

    public function down()
    {
        Schema::dropIfExists('tnt_job_options');
        Schema::dropIfExists('tnt_job_option_entity');
        Schema::dropIfExists('tnt_job_location_entity');
        Schema::dropIfExists('tnt_job_nationality_entity');
    }
}
