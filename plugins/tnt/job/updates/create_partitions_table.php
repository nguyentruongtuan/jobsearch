<?php namespace TNT\Job\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class CreatePartitionsTable extends Migration
{
    public function up()
    {
        Schema::create('tnt_job_partitions', function(Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->integer('api_id');
            $table->string('name');
            $table->string('company_id');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('tnt_job_partitions');
    }
}
