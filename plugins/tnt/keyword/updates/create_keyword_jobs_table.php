<?php namespace TNT\Keyword\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class CreateKeywordJobsTable extends Migration
{
    public function up()
    {
        Schema::create('tnt_keyword_keyword_jobs', function(Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->integer('job_id');
            $table->integer('keyword_id');
            $table->primary(['job_id', 'keyword_id']);
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('tnt_keyword_keyword_jobs');
    }
}
