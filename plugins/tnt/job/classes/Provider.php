<?php

namespace TNT\Job\Classes;


use Doctrine\Common\Proxy\Exception\UnexpectedValueException;
use GuzzleHttp\Exception\BadResponseException;
use League\Flysystem\Exception;
use League\OAuth2\Client\Provider\Exception\IdentityProviderException;
use League\OAuth2\Client\Provider\GenericProvider;
use October\Rain\Support\Facades\Flash;
use Psr\Http\Message\RequestInterface;
use Psr\Http\Message\ResponseInterface;
use League\OAuth2\Client\Token\AccessToken;
use TNT\Job\Classes\Mapping\Mapping;
use TNT\Job\Models\Settings;

class Provider extends GenericProvider {

	private $responseCode;
	public $token;
	public $endpoint = 'https://api-hrbc-jp.porterscloud.com/v1/';
	public $options;

	public function __construct( array $options = [], array $collaborators = [] ) {

		$options = [
			'clientId'                => Settings::get('client_id'),    // The client ID assigned to you by the provider
			'clientSecret'            => Settings::get('client_secret'),   // The client password assigned to you by the provider
			'redirectUri'             => url('/tnt/callback'),
			'urlAuthorize'            => Settings::get('authorize_url'),
			'urlAccessToken'          => Settings::get('token_url'),
			'urlResourceOwnerDetails' => Settings::get('owner_url'),
			'scopes' => [
				'user_r',
				'partition_r',
				'field_r',
				'option_r',
				'client_r',
				'recruiter_r',
				'job_r',
				'candidate_r',
				'resume_r',
				'process_r',
				'attachment_r',
			]
		];

		$this->options = $options;

		if (Settings::get('access_token')) {
			$token = new AccessToken([
				'access_token' => Settings::get('access_token'),
				'refresh_token' => Settings::get('refresh_token'),
				'expires' => (int)Settings::get('expires')
			]);

			$this->token = $token;
			if ($this->token->hasExpired()) {
				// refresh the token
				trace_log('Token expired');
			}
		}


		parent::__construct( $options, $collaborators );
	}

	/**
     * Extend GenericProvider
     */

	/**
	 * Returns authorization parameters based on provided options.
	 *
	 * @param  array $options
	 * @return array Authorization parameters
	 */
	protected function getAuthorizationParameters(array $options)
	{
		if (empty($options['state'])) {
			$options['state'] = $this->getRandomState();
		}

		if (empty($options['scope'])) {
			$options['scope'] = $this->getDefaultScopes();
		}

		$options += [
			'response_type'   => 'code',
			'approval_prompt' => 'auto'
		];

		if (is_array($options['scope'])) {
			$separator = $this->getScopeSeparator();
			$options['scope'] = implode($separator, $options['scope']);
		}

		// Store the state as it may need to be accessed later on.
		$this->state = $options['state'];

		// Business code layer might set a different redirect_uri parameter
		// depending on the context, leave it as-is
		if (!isset($options['redirect_uri'])) {
			$options['redirect_url'] = $this->redirectUri;
		}

		$options['app_id'] = $this->clientId;

		return $options;
	}

	/**
	 * Requests an access token using a specified grant and option set.
	 *
	 * @param  mixed $grant
	 * @param  array $options
	 * @return AccessToken
	 */
	public function getAccessToken($grant, array $options = [])
	{
		$grant = $this->verifyGrant($grant);

		$params = [
			'app_id'     => $this->clientId,
			'secret' => $this->clientSecret,
			'redirect_url'  => $this->redirectUri,
		];

		$params   = $grant->prepareRequestParameters($params, $options);
		$request  = $this->getAccessTokenRequest($params);
		$response = $this->getParsedResponse($request);
		$prepared = $this->prepareAccessTokenResponse($response->getArray());
		$token    = $this->createAccessToken($prepared, $grant);

		return $token;
	}

	public function getRefreshToken() {
		$grant = $this->verifyGrant('refresh_token');
		$params = [
			'app_id'     => $this->clientId,
			'secret' => $this->clientSecret,
			'redirect_url'  => $this->redirectUri,
			'refresh_token'  => $this->token->getRefreshToken(),
		];

		$params   = $grant->prepareRequestParameters($params, $this->options);
		$request  = $this->getAccessTokenRequest($params);
		$response = $this->getParsedResponse($request);
		$prepared = $this->prepareAccessTokenResponse($response->getArray());
		$token    = $this->createAccessToken($prepared, $grant);
		return $token;
	}

	protected function prepareAccessTokenResponse( array $result ) {
		$mapping = [
			'AccessToken' => 'access_token',
			'AccessTokenExpiresIn' => 'expires_in',
			'RefreshToken' => 'refresh_token'
		];

		$content = [];
		foreach ($mapping as $key => $mapKey) {
			$content[$mapKey] = $result[$key];
		}

		return $content;
	}

	/**
	 * Attempts to parse a JSON response.
	 *
	 * @param  string $content JSON content from response body
	 * @return array Parsed JSON data
	 * @throws UnexpectedValueException if the content could not be parsed
	 */
	protected function parseJson($content)
	{
		// $content = simplexml_load_string($content);
		$xmlIterator = new \SimpleXMLIterator($content);
		$content = [];
		for ($xmlIterator->rewind(); $xmlIterator->valid() ; $xmlIterator->next()) {
			$content[$xmlIterator->key()] = $xmlIterator->getChildren()->__toString();
		}

		if (libxml_get_last_error() !== false) {
			throw new UnexpectedValueException(sprintf(
				"Failed to parse XML response: %s",
				libxml_get_last_error()
			));
		}

		return $content;
	}

	public function getParsedResponse( RequestInterface $request ) {
		try {
			$response = $this->getResponse($request);
		} catch (BadResponseException $e) {
			$response = $e->getResponse();
		}

		$class = explode('/', $request->getRequestTarget());
		$class = ucfirst(end($class));
		if (strpos($class, '?')) {
			$class = explode('?', $class)[0];
		}
		$class = 'TNT\\Job\\Classes\Mapping\\' . $class;

		/** @var $parsed Mapping */
		$parsed = new $class($response);

		$this->checkResponse($response, $parsed->getArray());

		return $parsed;
	}

	protected function parseResponse(ResponseInterface $response)
	{
		$content = (string) $response->getBody();
		$type = $this->getContentType($response);

		if (strpos($type, 'urlencoded') !== false) {
			parse_str($content, $parsed);
			return $parsed;
		}

		// Attempt to parse the string as JSON regardless of content type,
		// since some providers use non-standard content types. Only throw an
		// exception if the JSON could not be parsed when it was expected to.
		try {
			return $this->parseJson($content);
		} catch (UnexpectedValueException $e) {
			if (strpos($type, 'json') !== false) {
				throw $e;
			}

			if ($response->getStatusCode() == 500) {
				throw new UnexpectedValueException(
					'An OAuth server error was encountered that did not contain a JSON body',
					0,
					$e
				);
			}

			return $content;
		}
	}

	/**
	 * @inheritdoc
	 */
	protected function checkResponse(ResponseInterface $response, $data)
	{
		if (!empty($data['Error'])) {
			$error = $data['Message'];
			$code  = $this->responseCode ? $data[$this->responseCode] : 0;
			throw new IdentityProviderException($error, $code, $data);
		}
	}

	protected function getAuthorizationHeaders( $token = null ) {
		if ($token) {
			return ['X-porters-hrbc-oauth-token' => $token->getToken()];
		}
	}
}