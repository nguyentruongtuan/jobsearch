<?php namespace TNT\Job\Classes\Mapping;

class Partition extends BaseMapping {

	public function getAttributes() {
		return [
			'api_id' => "Partition.P_Id",
			'name' => "Partition.P_Name",
			'company_id' => "Partition.P_CompanyId",
		];
	}

	public function getArray() {
		return $this->array;
	}
}