<?php namespace Tnt\Job\Console;

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;
use TNT\Job\Classes\Provider;
use TNT\Job\Models\Field;
use TNT\Job\Models\Recruiter;
use TNT\Job\Models\Settings;

class Fetchrecruiter extends Command
{
    /**
     * @var string The console command name.
     */
    protected $name = 'job:fetchrecruiter';

    /**
     * @var string The console command description.
     */
    protected $description = 'No description provided yet...';

    /**
     * Execute the console command.
     * @return void
     */
    public function fire()
    {
	    $provider = new Provider();
	    $partition = Settings::get('active_partition');

	    $activeFields = Field::query()->select('*')->where('active', true)->where('resource_type', 9);

	    $fieldArr = $activeFields->lists('alias');
	    $fieldArr[] = 'Recruiter.P_Id';

	    for ($i =0 ; $i < 3; $i++) {
		    $ids = [];
		    $items = Recruiter::where('is_fetch', false)->take(10)->get();
		    foreach ($items as $item) {
			    $ids[] = $item->id;
		    }

		    if (!count($ids)) {
			    return;
		    }

		    $query = [
			    'partition=' . $partition,
			    'condition=Recruiter.P_Id:or=' . implode(':', $ids),
			    'field=' . implode(',', $fieldArr)
		    ];
		    $url = $provider->endpoint . 'recruiter?' . implode('&', $query);
		    $request = $provider->getAuthenticatedRequest('GET', $url, $provider->token);
		    $response = $provider->getResponse($request);

		    $body = $response->getBody()->getContents();
		    $body = simplexml_load_string($body);
		    $body = \GuzzleHttp\json_decode(\GuzzleHttp\json_encode($body));

		    if (in_array($body->Code, ['401'])) {
			    echo 'Cannot get data Code:' . $body->Code;
			    return;
		    }

		    if (!$body->Item) {
			    return;
		    }

		    foreach ($body->Item as $apiItem) {
			    $job = $items->where('id', (int)$apiItem->{'Recruiter.P_Id'})->first();
			    if (!$job) {
				    continue;
			    }

			    $syncData = [];
			    foreach ($activeFields->get() as $field) {
				    $value = $apiItem->{$field->alias};
				    if (is_object($value)) {
					    for ($i=0 ; $i<5 ; $i++) {
						    $value = reset($value);
						    if (!is_object($value)) {
							    break;
						    }
					    }
				    }
				    $syncData[$field->id] = [
					    'value' => $value
				    ];
			    }
			    $job->fields()->sync($syncData);
		    }

		    \Db::table('tnt_job_recruiters')->whereIn('id', $items->lists('id'))->update(['is_fetch' => true]);
	    }
    }

    /**
     * Get the console command arguments.
     * @return array
     */
    protected function getArguments()
    {
        return [];
    }

    /**
     * Get the console command options.
     * @return array
     */
    protected function getOptions()
    {
        return [];
    }
}
